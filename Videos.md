# Videos

Hier findet Ihr alle Videos des Kurses:  
https://tube.tchncs.de/c/pykurs_e/

## Allgemeine Hinweise

Versucht, die Videos "aktiv" anzuschauen:
- Vorher:
  - Überlegt vorher kurz, was Ihr über das Thema schon wisst.
  - Schreibt Euch eventuell konkrete Fragen auf, die Ihr schon habt.
- Beim Anschauen:
  - Spult zurück und schaut Euch Sachen mehrfach an, wenn sie schwierig zu verstehen sind.
  - Haltet ab und zu an und probiert Sachen aus, die im Video gezeigt werden, oder von denen Ihr Euch fragt: "Geht das eigentlich auch mit ... ?"
  - Wenn Ihr Schwierigkeiten habt, Euch zu konzentrieren, experimentiert mit der Abspielgeschwindigkeit. Manchmal hilft es total, das Video auf 1.5-facher Geschwindigkeit zu schauen, weil eins dabei nicht so leicht abgleiten kann... Oder auch nur auf 0.8-facher Geschwindigkeit.
- Danach:
  - Versucht, die wichtigsten Infos des Videos zusammenzufassen. Stellt Euch vor, Ihr erklärt das Eurem Selbst von vor 20 Minuten.
  - Besonders gut ist es, wenn Ihr Euch selbst Aufgaben überlegt, die Ihr mit dem neuen Wissen lösen könnt.
  - Wenn etwas ganz unverständlich war:
    - Schreibt gerne Piko eine Mail – wahrscheinlich seid Ihr nicht die einzigen...
    - Natürlich könnt Ihr auch bei unserem Treffen alle Fragen loswerden.
    - Oder Ihr recherchiert weitere Informationen im Internet.
- Variiert, ob Ihr zuerst die Aufgaben probiert oder zuerst das Video anschaut!