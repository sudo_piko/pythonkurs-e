## Python für absolute Anfänger*innen

---

# Ablauf
* Vorstellung Piko, Haecksen
* Läuft Thonny bei allen?
* Kurs-Ablauf
* Fragen
* Kleingruppen

----

# Haecksen
Gruppe von technikinteressierten FLINTA-Personen  
Mehr Informationen auf https://haecksen.org  
Lokale Gruppen unter: https://www.haecksen.org/lokale-gruppen/  


Umfeld des Chaos Computer Club

----

# Piko
hat das auch nicht studiert  
Musik, CCC, Hamburg, Haecksen

----

# Wer seid Ihr?
drei Infos über Euch in den Chat schreiben

----

# Wau-Holland-Stiftung
Umfeld des Chaos Computer Club  
Mehr Informationen auf https://wauland.de

---

# Thonny


```python
print("Hallo Welt!")
import turtle
```
***


(in den unteren Fensterteil "Kommandozeile", 
nach `>>>`)

Schreibt diese zwei Zeilen nacheinander in Euren Thonny und drückt Enter nach jeder Zeile. Gibt es eine rote Schrift und irgendwo steht Error? Wenn ja:
- Habt Ihr es wirklich richtig geschrieben?
- Steht am Anfang "Warning" und Ihr habt einen Mac? Dann dürft Ihr es erstmal ignorieren.
- Wenn trotz richtiger Schreibung trotzdem ein Error auftaucht, dann ist da wahrscheinlich irgendwas faul... Kopiert das Ganze in eine Mail und schickt sie an Piko. 

---

    
## Ablauf

    
dienstags 19:30 bis 21:00

- Input per Video
- Hausaufgaben
- evtl. Treffen in Kleingruppen
- Kurstermine im BBB

zusätzlich: Projekte


![](https://md.ha.si/uploads/58773371-018d-472e-8f1f-53c31dcbeb42.png)

---

## Inverted Classroom

https://youtu.be/KWRB072USmI?si=w3A4PG949VXddLuZ&t=610
(Minute 10 bis 27)

---

## Python als Taschenrechner

(Siehe Video 1.2)

---

# Adressen

Fragen: piko@riseup.net

Aufgaben und Protokolle: 
https://gitlab.com/sudo_piko/pythonkurs-e

----

## Gitlab

Diese Webseite hier. Da finden sich alle Infos und Links.

----
    
## Ablauf
    
dienstags 19:30 bis 21:00

- Input per Video
- Hausaufgaben
- evtl. Treffen in Kleingruppen
- Kurstermine im BBB

zusätzlich: Projekte

---

# Kleingruppen

* 3 bis 5 Personen
* wöchentlich treffen

Einteilung später

---

# Thonny anwerfen

Turtle durch die Gegend laufen lassen. Siehe Video 1.3

---

# Trivia

Name mit E

---

# Generative Art

https://www.youtube.com/watch?v=meqHPIOzk-U

Wikipedia: "Das Werk oder Produkt entsteht durch das Abarbeiten einer prozessualen Erfindung, das heißt, eines vom Künstler geschaffenen Regelsatzes bzw. eines Programmes"

----

### NFTs und Generative Art

Line goes up: https://www.youtube.com/watch?v=YQ_xWvX1n9g
Generative Art und NFTs: https://jugendhackt.org/blog/community-talk-19-nft-crypto/

----

### Künstliche Intelligenz

Wikipedia: "Das Werk oder Produkt entsteht durch das Abarbeiten einer prozessualen Erfindung, das heißt, eines vom Künstler geschaffenen Regelsatzes bzw. eines Programmes"

---

# Fragen?

---

# Kleingruppen

* 3 bis 5 Personen
* wöchentlich treffen

----

## Aufgaben in den Kleingruppen

(in aufsteigender Wichtigkeit)
* Hausaufgaben besprechen (möglichst vorher schon gemacht haben)
* Unklares aus Stunde besprechen
* Spaß haben, sich gut vertragen

----

## Aufgaben für erstes Treffen

zusätzlich: Vorstellungsrunde, zB:
* vier wichtige Bereiche
* was wollt Ihr vom Kurs

----

## Gruppeneinteilungsballett

* den Namen (wie in BBB) in den *einen* präferierten Termin schreiben
* Piko sortiert
* E-Mail-Adressen austauschen per privater Nachricht im BBB (möglichst jede* an jede*) 
* per Mail erstes Treffen verabreden

---

# ENDE

Piko bleibt noch für technische Fragen und Probleme
