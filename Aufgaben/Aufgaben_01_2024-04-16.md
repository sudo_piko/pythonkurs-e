# Aufgaben vom 16.04.2024

Zwei Hinweise vorweg:
* Wenn hier etwas für Euch unverständlich oder falsch ist, schreibt mir gerne eine Mail! Ihr seid höchstwahrscheinlich nicht die Einzigen, für die es unverständlich ist. Außerdem macht Piko wie alle immer mal wieder Fehler und übersieht Sachen. Mit Euren Fragen helft Ihr auch den anderen!
* Wenn eine Hausaufgabe mal zu schwierig für Euch ist, schaut sie Euch genau an und versucht, zu beschreiben, wo es hakt. Wenn es nach der Besprechung in der Kleingruppe immer noch nicht funktioniert, könnt Ihr das in der kommenden Stunde ansprechen oder Piko gleich eine Mail schreiben.

## 0 - Lesestoff
Hier gibt es immer mal Sachen, die interessant oder unterhaltsam sein könnten oder den Stoff noch einmal gut veranschaulichen. Oft sind es tatsächlich Texte, manchmal aber auch Videos.
Die Sachen, die ich hier verlinke, werden aber nie notwendig sein, um im Kurs mitzukommen. 

### 8 Things I Wish I Knew When I Started Programming
https://yewtu.be/watch?v=LZyDLtXb1-Q

Für uns wichtig: Nr. 2, 4, 5, 7  
Und macht Euch keine Sorge wegen der Mathematik. Das ist für später.

### Was ist generative Kunst?

https://www.amygoodchild.com/blog/what-is-generative-art

## 1 – Python als Taschenrechner verwenden

Verwendet die Kommandozeile als Taschenrechner!
Rechnet ein paar Sachen aus, zum Beispiel:
* 456+789
* 456-789
* 456/789
* 2 * (3 + 8)
* Gibt es einen Unterschied zwischen `2*4` und `2.0*4`? Wenn ja, welchen?
* Gibt es einen Unterschied zwischen `2 * 4` (mit Leerzeichen) und `2*4` (ohne Leerzeichen)? Wenn ja, welchen?
Die folgenden Sachen sind vielleicht nicht so offensichtlich, aber Ihr könnt sie durch Probieren herausfinden: Verändert die Zahlen und überlegt, was die Operatoren wohl so machen.
* 456**4
* 23//7
* 23%7

Wenn Ihr Euren Computer überfordert, könnt Ihr jederzeit oben auf die rote Stopp-Schaltfläche oben in der Symbolleiste klicken, die das Programm abbricht.

## 2 – Turtle
Bleiben wir erstmal noch auf der Kommandozeile...

Wir hatten in der Stunde und im Video einige Funktionen der Turtle kennengelernt:
* `forward(zahl)` oder `fd(zahl)`: Bewegt die Turtle um `zahl` Pixel nach vorn – also zB: forward(100) bewegt die Turtle um 100 Pixel nach vorn.
* `right(zahl)` oder `rt(zahl)`: Dreht die Turtle um `zahl` Grad nach rechts
* `left(zahl)` oder `lt(zahl`: Dreht die Turtle um `zahl` Grad nach links

Um mit der Turtle Sachen malen zu können, müsst Ihr sie vorher importieren. Das heißt, ihr müsst als Erstes tippen:  
`from turtle import *`
Das sollte dann so aussehen:
```
>>> from turtle import *
>>>
```

:zap: Lasst die Turtle ein Quadrat malen!

### 2.4 Fremder Code
Jetzt verlassen wir die Kommandozeile und schreiben ein Programm oder Skript.

Kopiert folgendes kurzes Programm in Euren Thonny.
```python
from turtle import *
for i in range(5):
    forward(100)
    left(45)
```

<details> 
  <summary>Tipps: Wohin nochmal?</summary>
Da, wo in der Abbildung "Das hier ist ein Programm oder Skript" steht :)

![Fenster](../Abbildungen/Thonny_Fenster.png)

</details>

* Führt es aus, indem Ihr auf das Play-Zeichen in der Symbolleiste von Thonny klickt (grüner Kreis mit einem weißen Dreieck).
* Was malt dieser Code? Wie hättet Ihr das Ergebnis bisher hergestellt? 
* Was passiert, wenn Ihr die Zahl von der `range()`-Funktion variiert? Probiert es aus! (Also wenn Ihr statt der `5` eine `10` schreibt oder so –  oder eine `2`)
* Was passiert, wenn Ihr die beiden anderen Zahlen variiert? Stellt eine Vermutung auf, bevor Ihr es ausprobiert!
* Wie müsste das Programm aussehen, um Euch ein Quadrat zu malen?

## 3 – Kleingruppen
### Aufgaben in den Kleingruppen

(in aufsteigender Wichtigkeit)
* Hausaufgaben besprechen (möglichst vorher schon gemacht haben)
* Unklares aus Stunde besprechen
* Spaß haben, sich gut vertragen

### Aufgaben für erstes Treffen

zusätzlich: Vorstellungsrunde, zB:
* vier wichtige Bereiche
* was wollt Ihr vom Kurs