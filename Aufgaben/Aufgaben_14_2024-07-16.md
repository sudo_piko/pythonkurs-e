# Aufgaben vom 16.07.2024 – Woche 14




1. Schaut Euch die Videos zur Woche 14 an: https://tube.tchncs.de/c/pykurs_e/
2. Macht das Quiz: https://pythonkurs.ithea.de/
3. Versucht Euch hier an den Aufgaben!

## 0 – Lesestoff

### Aktuelles

Lilith Wittmann hat mal wieder etwas gecybert:  
https://lilithwittmann.medium.com/festnetz-cool-got-acquired-by-x-forward-for-ventures-der-deutschen-telekom-ag-d3fbe59f7365  

Recherche zu Datenverkauf durch Werbefirmen:  
https://netzpolitik.org/2024/databroker-files-firma-verschleudert-36-milliarden-standorte-von-menschen-in-deutschland/

### Firefox

Warum auch die "datenschutzfreundlichen" Werbe-Messungen ein Problem sind, und wie sie abgeschaltet werden können:  
https://netzpolitik.org/2024/privatsphaere-firefox-sammelt-jetzt-standardmaessig-daten-fuer-die-werbeindustrie/

### Datenschutzfreundlichere Alternativen zu beliebten Apps

https://www.kuketz-blog.de/empfehlungsecke/#android

Bisschen ausführlicher in einer dreiteiligen Artikelserie:  
https://www.kuketz-blog.de/your-phone-your-data-light-android-unter-kontrolle/

### Cheat sheet zu den Importen

[Importe_Cheatsheet.pdf](..%2FCheatsheets%2FImporte_Cheatsheet.pdf)




## 1 – Weisheiten generieren

:zap: Generiert Zufallsweisheiten nach diesem Baugerüst: XXX sind wie YYY: sie ZZZ.    

Also zum Beispiel: `Freunde sind wie Koffer: sie machen noch keinen Sommer.`


Für die Wörter könnt Ihr folgende Listen verwenden, aber Ihr seht sicher schon, dass die nicht gut sortiert sind – da müsst Ihr also auch noch etwas putzen...

``` 
Wünsche
Träume
Ängste
Ziele
Freunde
Hoffnungen
Fragen
Antworten
Träume, 
Erfahrungen, 
Geschichten, 
Missgeschicke, 
Zähne, 
Freunde, 
Verwandte, 
 Kinder, 
 Geister, 
Bücher, 
Koffer, 
Betten, 
Essen, 
Käse, 
Gesetze, 
Fässer, 
Weine, 
Haare, 
Beine, 
Klischees, 
```

```
gehen auf
frieren im Winter
krümeln
brauchen Schokolade
haben keine Ahnung
fallen nicht weit vom Stamm
machen allein auch nicht glücklich
ersetzen einen Zimmermann
wurden von der Werbeindustrie frei erfunden
heilen alle Wunden
werden gern unter Freunden geteilt
machen noch keinen Sommer
werden auch von Vögeln gemieden
fügen sich ganz von allein
ergeben einen schmackhaften Auflauf
haben nur andere Vorzeichen
```
Wenn Ihr weitere Wörter habt, dann hängt sie gerne hier an:  
https://md.ha.si/A7GOJOnySK-1cMMHgmnjKQ?both

<details> 
  <summary>Tipps </summary>
   Überlegt Euch, welche Schritte Ihr machen müsst, und sucht im Internet, welche Funktionen oder Methoden Ihr dafür braucht.
</details>

<details> 
  <summary>Tipps: Welche Schritte </summary>
    Ihr müsst die Liste aufteilen, die störenden Zeichen loswerden, Wörter auswürfeln, sie zusammenfügen und printen. 
</details>


<details><summary>Wie kann ich die Wörterliste putzen?</summary>
Da sind Leerzeichen, die wir da nicht brauchen können, und nach manchen Wörtern kommen Kommata. Für die Leerzeichen könnt Ihr direkt
die Methode `strip()` verwenden. Für die Kommata auch – aber da braucht es dann noch ein Argument.</details>


## 2 – Importe 
### 2.1 Korrigieren
Folgende Importe sind fehlerhaft. :zap: Korrigiert sie, sodass sie keine Fehlermeldungen mehr verursachen.

```python
import random

print(randint(1, 10))
```

```python
from random import randint
print(random.randint(1, 10))
```

```python
from turtle import *
turtle.forward(100)
```

### 2.2 Ergänzen
Bei den folgenden Programmen fehlen Importe. :zap: Ergänzt sie, sodass die Programme laufen.

```python

print(randint(1, 10))
```

```python

seite = 20
diagonale = math.sqrt(2) * seite  # sqrt = square root = Wurzel
```

```python

temperatur = random.random()*30
print("Und nun: der Wetterbericht. Es werden " + str(temperatur) + " Grad.")
```

```python

turtle.forward(200)
turtle.right(90)
turtle.forward(100)
```

```python

forward(200)
left(135)
forward(math.sqrt(2)*200)
left(135)
forward(200)
```

### 2.3 Eastereggs

Importiert das Modul `this`:

```python
import this
```
:zap: führt das Programm mit nur dieser einen Zeile aus. 

:zap: Importiert ebenso das Modul `antigravity`.