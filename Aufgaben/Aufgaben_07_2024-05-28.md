# Aufgaben vom 28.05.2024 – Woche 07




1. Schaut Euch die Videos zur Woche 7 an: https://diode.zone/c/pykurs_e/
2. Macht das Quiz: https://pythonkurs.ithea.de/
3. Versucht Euch hier an den Aufgaben!



## 0 – Lesestoff


### Glitchgallery

Beim Lösen der Aufgaben wird die Turtle immer mal wieder etwas machen, das Ihr so nicht erwartet habt. Manchmal wird es aber trotzdem interessant aussehen oder Euch auf neue Ideen bringen. Die Glitch Gallery ist ein Projekt, das die Ergebnisse von solchen Situationen, in denen Software nicht das ausgibt, was eins eigentlich erwartet, sammelt:  
https://glitchgallery.org/

### Parametrisierte Schneeflocke

Hier hat jemand das, was wir mit Mandalas machen, mit einer Schneeflocke gemacht:  
https://www.misha.studio/snowflaker/




## 1 – Würfeln

### 1.1 Würfelspiel

Baut ein Programm, das für Euch mit einem gewöhnlichen Würfel würfelt – es soll also eine Zufallszahl zwischen 1 und 6 ausgeben. Wenn es eine 6 gewürfelt hat, soll es zusätzlich noch "Yay! Gewonnen!" ausgeben.

<details> 
  <summary>Tipps </summary>
   Lasst das Programm erstmal das Würfelergebnis printen. Wenn das funktioniert, dann erst geht an das "Yay!...": Höchstwahrscheinlich habt Ihr das ja in einer Variable gespeichert; diese Variable könnt Ihr mittels `if ...` mit der 6 vergleichen. Schaut Euch dazu das Video zu if/else an, wenn Ihr nicht mehr wisst, wie das geht.
</details>

### 1.2 Wahrscheinlichkeit

Stellt Euch vor, Ihr hättet einen hundertseitigen Würfel – welche Zahlen wären dann anders in dem Programm?
Da habt Ihr natürlich weniger Gewinnchancen, wenn Ihr jetzt eine 100 Würfeln müsstet... :zap: Verändert das Programm so, dass Ihr "Yay! Gewonnen!" bekommt, wenn Ihr irgendwas über 90 würfelt.

Welche Wahrscheinlichkeit in Prozent habt Ihr, zu gewinnen?

<details> 
  <summary>Tipps </summary>
   Statt des Vergleichs mit `==` müsst Ihr jetzt mit `<` oder `>` vergleichen. Und Ihr müsst natürlich mit `90` vergleichen.
</details> 


### 1.3 Immer wieder...

Nehmt nochmal das Programm, das Ihr in Aufgabe 1.1 geschrieben habt (oder schreibt es nochmal, das ist eine gute Übung!)

:zap: Wenn Ihr es zehnmal würfeln lassen wollt, welche Zeilen müssen dann unbedingt in die for-Schleife? Probiert ein bisschen herum!

## 2 – Punktekreis

### 2.1 Gepunktete Linie

Schreibt ein Programm, das mit einer for-Schleife und `dot()` eine gepunktete Linie malt:  
![W7_Punktelinie.png](..%2FAbbildungen%2FW7_Punktelinie.png)

### 2.2 Manche Punkte 

In jeder einzelnen for-Schleife, bevor Ihr mit `dot()` den Punkt malt, könnt Ihr würfeln, so wie in Aufgabe 1.1.
1. :zap: Wenn Ihr eine 6 würfelt, dann lasst das Programm "Huiiiiii!" ausgeben.
2. :zap: Jetzt zieht den Aufruf von `dot()` mit in die if-Verzweigung rein: Nur, wenn eine 6 gewürfelt wird, soll das Programm einen Punkt malen. Das Ziel ist, die Linie auf "gleichmäßige" Art und Weise unregelmäßig zu machen...

<details> 
  <summary>Tipps: "Mit in die if-Verzweigung reinziehen?"</summary>
   Ob das "in" der if-Verzweigung ist oder nicht, hängt ja von den Einrückungen ab. Wenn Ihr also den `dot()`-Aufruf in die if-Verzweigung reinziehen wollt, dann müsst Ihr ihn um vier Leerzeichen (oder ein Tab) mehr einrücken, genauso wie bei for-Schleifen.
</details>

### 2.3 Kreis

:zap: Lasst die Turtle am Ende jedes for-Schleifen-Durchlaufs um 10 Grad drehen. So entsteht ein Kreis (oder ein Bogen) aus Punkten.

:zap: Wie viel Grad Drehung braucht es, damit es genau ein Kreis wird? Verändert das Programm so, dass es ein Kreis wird.

<details> 
  <summary>Tipps: Wie viel Grad? </summary>
    Am Ende soll sich die Turtle ja um 360 Grad gedreht haben. Das macht sie in gleichgroßen Stückchen; wenn Ihr zB `for i in range(20):` habt, dann sind es 20 Stückchen. Ihr müsst also einfach 360 durch 20 teilen. (Die 20 könnt Ihr aber auch in eine Variable tun und es dann Python ausrechnen lassen – wieder eine Parametrisierung, die Euch das Leben leichter macht!)   
</details>

### 2.4 Funktion und Mandala

:zap: Macht aus diesem unregelmäßig gepunkteten Kreis eine Funktion und verwendet sie in einem Mandala!



## Optional: Sich auf der Leinwand orientieren (zum Einüben)
(Es gibt zwei Typen optionaler Aufgaben: "zum Einüben" (für Leute, die gerne mehr Übung hätten und noch Schwierigkeiten mit den anderen Aufgaben haben) und "zum Weiterdenken" (etwas anspruchsvoller; für Leute, die gerne die Konzepte vertiefen und erweitern wollen).)

Damit Ihr die Turtlesteuerung ein bisschen besser in die Finger kriegt, könnt Ihr Programme schreiben, die folgende Dinge malen:
- ein Peace-Zeichen
- eine Schneeflocke
- eine Sonne auf hellblauem Hintergrund (oder den Hintergrund als Farbverlauf :D)
- eine Blume (das darf ein langes Programm werden, mit Stängel, Blatt, Blüte und so weiter...)

Ihr braucht dafür keine for-Schleifen verwenden, aber bei den letzten drei könnten sie nützlich sein.