# Hausaufgabe 3: 2024-04-30 kommentiert von mayz
# die Themen heute sind
# 1. for-schleifen
# 2. debugging

print("Hausaufgabe 3: for-Schleifen, Regenbogendebuggen, Lizenzen\n")

print("1.1 for-Schleife mit print")

# resultat: 0-7
# da wir bei 0 anfangen zu zählen, erhalten wir 0,1,2,3,4,5,6,7 also insgesamt 8 Zahlen da range(8) 
for i in range(8):
    print(i)
    
# resultat: 0.1 - 0.7
for i in range(8):
    print(i/10) # teilen durch 10
    
# resultat: 0,2,4,6,8, ..., 14
for i in range(8):
    print(i*2) # multipliziert mit 2
    
# resultat: 0, 3, 9, ..., 21
for i in range(8):
    print(i*3) # multipliziert mit 3
    
# result:0, 0, 0, 1, 1, 1, ..., 7, 7, 7
# jetzt wird es ein bisschen komplizierter
# probiert gerne mal aus, was passiert, wenn ihr range(3) und range(8) tauscht?

for i in range(8): # wir wollen wieder die Zahlen 0-7
    for n in range(3): # aber diesmal drei mal hintereinander
        print(i)
        
print("1.2: for-Schleife mit der Turtle\n")

# schreibe ein Programm, das mittels einer for-Schleife eine gestrichelte Linie malt
from turtle import *
# wir bekommen keinen Beispielcode von Piko, wie können wir also an die Aufgabe rangehen?
# das hier sind Gedanken, die ich mir gemacht habe:

# wir brauchen auf jeden Fall penup(), um uns nach vorne zu bewegen ohne zu Malen
# folglich brauchen wir dann auch wieder pendown() um danach zu malen
# wir können das in eine for-Schleife packen, dann müssen wir nicht so viele Zeilen Code schreiben:
# wir müssen nach vorne,
# dann "den Stift absetzen",
# wieder nach vorne,
# und "den Stift wieder aufsetzen"
# als Code sieht das dann so aus:

for i in range(10):
    # der Code-Block nach der for-Schleife muss eingerückt werden
    forward(20)
    penup()
    forward(20)
    pendown()
reset()
        
print("1.3: for-Schleife für das bunte Quadrat\n")
# Der Code fürs Dreieck:

# from turtle import *
# pensize(10)
# for farbe in ("red", "green", "blue"):
#     color(farbe)
#     forward(100)
#     left(120)

# für ein Viereck brauchen wir 4 Farben in der Liste, d.h wir fügen "yellow" zur Liste hinzu
# außerdem brauchen wir einen anderen Winkel
# für ein Quadrat sind das 90 Grad, also left(90)
# das Ergebnis sieht dann so aus:

pensize(10)

for farbe in ["red", "green", "blue", "yellow"]: # Gelb hinzugefügt
    color(farbe)
    forward(100)
    left(90) # 90 Grad-Winkel fürs Quadrat
reset()
    
print("2. Debugging\n")

# es gibt verschiedene Herangehensweisen für das Debugging
# ihr könnt entweder Piko's Code nehmen, kopieren und dann nacheinander die Fehlermeldungen durchgehen
# oder ihr versucht im Voraus so viele Fehler wie möglich zu finden und zu korregieren
# es folgt Piko's fehlerhafter Code als Kommentare, ich werde "##" vor die Erklärung was falsch ist setzen

# from * import turtle
## "turtle" ist ein sog. Modul und enthält verschiedene Befehle z.b forward(), color(), etc
## "from (beliebiges Modul) import ..." bedeutet "aus (beliebiges Modul) importiere ..."
## * bedeutet, dass wir das gesamte Modul importieren
## die richtige Reihenfolge ist:
## from turtle import *

# regenbogenfarben = ["purple", "blue", "green", "orange", "red"
## wir definieren hier eine Variable namens Regenbogenfarben
## der Inhalt der Variable ist eine Liste mit Farbnamen
## man kann also sagen, wir geben der Liste den Namen Regenbogenfarben
## in Python schreiben wir Listen in eckige Klammern [...]
## oben fehlt die eckige geschlossene Klammer: ]
## Korrekt sieht das dann so aus:
## regenbogenfarben = ["purple", "blue", "green", "orange", "red"]

# pensize 20
## pensize(), forward(), color(), etc sind sog. Funktionen
## genauso wie print()
## auf sie folgt immer ein normales Klammernpaar: ()
## in der Klammer steht dann, was geprintet wird, welche Farbe, etc
## korrekt ist also:
## pensize(20)

# for farbe in regenbogenfarbe
## wir haben hier eine for-Schleife
## sie sagt aus: " for <schleifenvariable> in <wertevorrat>: führe den eingerücken Code-Block aus"
## danach folgt immer ein Doppelpunkt : (der fehlt bei Piko)
## der Codeblock, der dann folgt wird eingerückt

## hier definieren wir außerdem die <(Schleifen-)Variable>: farbe
## unser <Wertevorrat> ist die Liste, die wir oben regenbogenfarben genannt haben

## unser Ziel ist es, eine Farbe aus dem Wertevorrat zu nehmen, eine Linie zu zeichnen
## und das untereinander mit jeder Farbe in der Liste zu wiederholen
## wie wir die Turtle bewegen um das schön untereinander zu haben, seht ihr unten
# forward(200)
#     
# penup()
# back(100)
# left(90)
# forward(20)
# right(90)
# pendown()

# hier der richtige Code für die Regenbogenflagge:
regenbogen = ["purple", "blue", "green", "yellow", "orange", "red"]

pensize(20)

for farbe in regenbogen:
    color(farbe)
    forward(150)
    penup()
    forward(-150)
    right(90)
    forward(20)
    left(90)
    pendown()