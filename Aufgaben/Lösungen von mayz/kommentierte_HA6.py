# Hausaufgabe 6: 2024-05-21 kommentiert von mayz
# die Themen heute sind:
# 1. Modulo
# 2. Passwortabfrage
# 3. Besondere Zahlen
# 4. Noch ein Fehler
# 5. Optional: for-Schleifen üben

from turtle import *

# kurze Erklärung zum Modulo rechnen:
# es gibt uns den Rest der Division
# 5/5 lässt sich ohne Rest teilen, der Rest = 0 d.h 5 % 5 = 0
# 6/5 lässt sich nicht ohne Rest teilen, es ergibt 1 Rest=1 d.h 6 % 5 = 1
# 7/5 ergibt 1 Rest 2 d.h 7 % 5 = 2
# 8/5 ergibt 1 Rest 3 d.h 8 % 5 = 3
# 9/5 ergibt 1 Rest 4 d.h 9 % 5 = 4
# 10/5 lässt sich ohne Rest teilen, es ergibt 2 d.h 10 % 5 = 0

# das heißt, um zu überprüfen ob eine Zahl z.b durch 5 teilbar ist, müssen wir prüfen ob: Zahl % 5 == 0
# um zu überprüfen ob eine Zahl gerade oder ungerade ist, prüfen wir, ob diese durch 2 (oder irgendeiner anderen Zahl in der 2-er Reihe) teilbar ist
# falls ja, ist die Zahl gerade
# falls nein, ist die Zahl ungerade

for i in range(30): # d.h wir prüfen die Zahlen von 0-29
    if i % 7 == 0: # wenn diese durch 7 teilbar sind, dann:
        print("Pfui! Bäh!")
        
# im Zahlenraum 0-29 gibt es 4 Zahlen, die durch 7 teilbar sind: 7, 14, 21, 28
# d.h das if-Statement wird 4 Mal ausgeführt

# eine gerade Zahl ist durch 2 teilbar
# nach dem selben Prinzip wie oben machen wir jetzt eine if-Abfrage, ob die Zahl durch 2 teilbar ist:
for i in range(30):
    if i % 2 == 0:
        print(i, "Ui, eine gerade Zahl! Die mag ich gerne")
        # wir müssen i theoretisch nicht printen, ich finde aber es hilft um den Prozess zu verstehen
        
print("2. Passwortabfrage")

# wir wollen eine if-else Abfrage, die überprüft ob der User-Input dem richtigen Passwort entspricht
# dafür haben wir das richtige Passwort schon vorgegeben:
richtiges_passwort = "Pupsgemüse"
# wir müssen unseren input in einer Variablen speichern
# auch das gibt uns Piko netterweise schon vor:
passwort = input("Bitte Passwort eingeben: ")
# jetzt müssen wir noch überprüfen, ob der Input mit dem gesetzten Passwort übereinstimmt:
if passwort == richtiges_passwort: # falls User-Input dem Passwort entspricht, tu das:
    print("Zugang gewährt")
else: # falls der User-Input nicht dem Passwort entspricht, tu stattdessen dashier:
    print("Zugang verweigert")
    
print("3. Besondere Zahlen")    
# da wir zwei Mal nach Input fragen, müssen wir die jeweiligen Eingaben unter zwei verschiedenen Variablen speichern:    
# mit int() konvertieren wir unseren User-Input in einen Integer
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))
# das hier ist das Beispiel von Piko:
if hunde > 3 and katzen < 5: # falls Hunde größer als 3 UND Katzen kleiner als 5
    print("Beide Zahlen erfüllen die Anforderungen.")
else: # der folgende Block wird ausgeführt wenn die eine der Zahlen ihre Bedingung nicht erfüllt
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")
    
# kommen wir zu den anderen Beispielen:

# mehr als 3 Hunde UND mehr als 2 Katzen:
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))

if hunde > 3 and katzen > 2: 
    print("Beide Zahlen erfüllen die Anforderungen.")
else:
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")

# genau 1 Hund UND mehr als 10 Katzen:
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))

if hunde == 1 and katzen > 10: # hunde muss genau 1 sein UND Katzen größer als 10
    print("Beide Zahlen erfüllen die Anforderungen.")
else:
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")
    
# genau 1 Hund UND 0 Katzen:
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))

if hunde == 1 and katzen == 0: # hunde muss genau 1 sein und katzen genau 0
    print("Beide Zahlen erfüllen die Anforderungen.")
else:
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")
    
# mehr Katzen als Hunde
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))

if hunde < katzen: # jetzt prüfen wir nur noch ob eine Zahl größer ist, als die andere, daher brauchen wir kein 'and'
    print("Beide Zahlen erfüllen die Anforderungen.")
else:
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")
    
# mehr als 3 Hunde UND mehr katzen als hunde
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))

if hunde > 3 and katzen > hunde: # hunde muss größer als 3 sein UND katzen muss größer sein als hunde
    print("Beide Zahlen erfüllen die Anforderungen.")
else:
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")
    
# mehr als 30 hunde ODER mehr als 20 katzen
hunde = int(input("Bitte gib die Anzahl der Hunde ein: "))
katzen = int(input("Bitte gib die Anzahl der Katzen ein: "))

if hunde > 30  or katzen > 20: # ODER Abfragen mit or
    print("Beide Zahlen erfüllen die Anforderungen.")
else: # jetzt wird dieser Block nurnoch ausgeführt, wenn BEIDE Konditionen NICHT zutreffen
    print("Mindestends eine Zahl erfüllt die Anforderungen nicht")
    
print("4. Noch ein Fehler")    

# Fehlermeldung für forward():
# TypeError: forward() missing 1 required prositional argument: 'distance'

# Fehlermeldung für penup(19):
# TypeError: penup() takes 0 positional arguments but 1 was given

# Erklärung:
# beide Fehler geben einen sogenannten TypeError, d.h der 'Typ' den wir angeben ist falsch
# sowohl penup() als auch forward() sind Funktionen des Turtle-Moduls
# in die Klammern kommen die sogenannten "(Funktions-)Argumente", falls sie notwendig sind
# für penup() ist das nicht der Fall, die Klammer bleibt leer
# geben wir doch ein Argument stellt Python fest: da is eins zu viel, das ist falsch und wirft deshalb einen Fehler
# umgekehrt bei forward():
# forward() braucht ein Argument um zu spezifizieren, wie weit wir nach vorne gehen
# fehlt diese Zahl kommt der Fehler mit dem Hinweis, dass das Argument für die Distanz nicht gegeben wurde

print("Optional: for-Schleife üben")

# ein Sechseck:
for i in range(6): # 6 Ecken
    fd(200)
    right(360/6) # eine volle Drehung wären 360 Grad, beim Sechseck drehen wir uns nur um ein Sechstel dieses Werts
    
# Kreis ohne circle() zu verwenden
# Theoretisch kann man einen Kreis annähern indem man ihn als Vieleck mit unendlich vielen Ecken beschreibt
# für die Turtle reichen bei kurzen Strecken von forward() bereits 50 Ecken um wenig eckig, aber dafür sehr rund auszusehen:
for i in range(50):
    fd(20)
    left(360/50)
    
# perlenkette ohne circle()
# wir nutzen das selbe Prinzip wie für den Kreis ohne circle()
# die Perlen sind einfach nur dot() in großer pensize()
# dot() nimmt das Argument der pensize in der Klammer an 
for i in range(20): # also eine Perlenkette mit 20 dots bzw "Perlen"
    dot(30)
    left(360/20)
    fd(50)
    