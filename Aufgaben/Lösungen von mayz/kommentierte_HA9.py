# Hausaufgabe 9 2024-06-11 kommentiert von mayz
# die Themen heute sind:
# 1. Fremder Code
# 2. Truchet Tiles
# Optional: Kopfrechenübungsprogramm

print("Hausaufgabe 9: Tilings: Zufall und TT, Kopfrechenübungsprogramm")
print("1. Fremder Code")

# wir kopieren mal Pikos Code:

from random import randint
# wir haben randint() importiert, also bauen wir irgendwo einen Zufall ein 
from turtle import *

pensize(2)
seitenlaenge = 40
speed(0)
pu()


def quadrat():
    # zeichnet ein Quadrat
    for i in range(4):
        fd(seitenlaenge)
        lt(90)

def kreis_im_quadrat():
    # zeichnet ein  Quadrat mit einem Kreis
    pd()
    quadrat()            
    fd(seitenlaenge/2)
    circle(seitenlaenge/2)
    back(seitenlaenge/2)
    pu()
    
def quadrat_mit_halbkreisblume():
    # zeichnet ein Quadrat mit einer Blume aus Halbkreisen
    pd()
    quadrat()
    for i in range(4):
        circle(seitenlaenge/2, 180)
        left(90)
    pu()
    
def leeres_quadrat():
    # malt ein leeres Quadrat
    pd()
    quadrat()
    pu()


for j in range(5): # j entspricht "Höhe"
    for i in range(7): # i entspricht "Breite"
        # was ist in zufallszahl drin?
        zufallszahl = randint(1, 3) # Zahl zwischen 1 und 3
        # randint() gibt uns eine random Zahl zwischen 1 und 3
        # in HA7 Aufgabe 1.1 hab ich das schonmal erklärt :)
        # da wir das IN der for-Schleife machen, würfeln wir in jedem Durchlauf eine neue Zahl 
        
        # lasst euch zufallszahl printen:
        print(zufallszahl)
        if zufallszahl == 1: # wenn zufallszahl == 1, dann:
            kreis_im_quadrat()
        if zufallszahl == 2: # wenn zufallszahl == 2, dann:
            quadrat_mit_halbkreisblume()
        if zufallszahl == 3: # wenn zufallszahl == 3, dann:
            leeres_quadrat()
        fd(seitenlaenge)
    fd(-seitenlaenge*7) # Breite * Anzahl der Tiles zurückgehen 
    lt(90)
    fd(seitenlaenge)
    rt(90)
reset()

# jetzt fügen wir mal noch ein paar andere Tiles hinzu:

speed(0)
seitenlaenge = 52
# diese Zahl muss durch 4 teilbar sein, sonst kommen Rundungsfehler auf und es verschiebt sich leicht
stiftdicke = 1
pensize(stiftdicke)

halbseite = seitenlaenge/2
diagonale = (2**(1/2)) * seitenlaenge  # **(1/2) = "hoch 0.5" = "Wurzel aus" => Wurzel aus 2, mal seitenlänge
diagonalhalbe = (2**(1/2)) * ((seitenlaenge/2) - stiftdicke)
kreisgroesse = seitenlaenge/4


def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        
def diagonalquadrat():
    pd()
    quadrat()
    left(45)
    fd(diagonale)
    fd(-diagonale)
    right(45)
    pu()
    
def inspo1():
    pd()
    quadrat()
    for i in range(6):
        left(45)
        fd(diagonalhalbe)
        fd(-diagonalhalbe)
        right(45)
        fd(seitenlaenge/10)
    fd(seitenlaenge/2 - seitenlaenge/10)
    left(90)
    fd(seitenlaenge)
    left(90)
    for i in range(6):
        left(45)
        fd(diagonalhalbe)
        fd(-diagonalhalbe)
        right(45)
        fd(seitenlaenge/10)
    fd(seitenlaenge/2 - seitenlaenge/10)
    left(90)
    fd(seitenlaenge)
    left(90)
    pu()
    
def quadrat_mit_halbkreisblume():
    pd()
    quadrat()
    for i in range(4):
        circle(seitenlaenge/2, 180)
        left(90)
    pu()
    
def inspo5():
    pd()
    quadrat()
    for i in range(2):
        for i in range(3):
            fd(kreisgroesse)
            left(90)
            fd(seitenlaenge)
            fd(-seitenlaenge)
            right(90)
        fd(kreisgroesse)
        left(90)
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    left(90)
    pu()

up()
goto(-400, -300)
down()

hoehe = 13
breite = 15
for j in range(hoehe): # j entspricht "Höhe"
    for i in range(breite): # i entspricht "Breite"
        # was ist in zufallszahl drin?
        zufallszahl = randint(1, 4)
        # print(zufallszahl)
        if zufallszahl == 1: # wenn zufallszahl == 1, dann:
            inspo5()
        if zufallszahl == 2: # wenn zufallszahl == 2, dann:
            quadrat_mit_halbkreisblume()
        if zufallszahl == 3: # wenn zufallszahl == 3, dann:
            inspo1()
        if zufallszahl == 4: # wenn zufallszahl == 4, dann:
            diagonalquadrat()
        fd(seitenlaenge)
    fd(-seitenlaenge*breite)
    lt(90)
    fd(seitenlaenge)
    rt(90)
reset()

    
print("2.Truchet-Tiles")

print("2.1 Funktionen bauen")
# der Code für die Truchet-Tiles mit gefüllten Diagonalen:
# ich weiß nicht so recht, wie ich meinen Gedankenganz erklären soll außer - es is immens hilfreich die Diagonale zu definieren
# falls ihr Fragen habt, stellt sie oder schickt euren Code in die Gruppe, wir können da bestimmt gemeinsam nach ner Lösung suchen
# ansonsten ist es viel trial-and-error

seitenlaenge = 50
halbseite = seitenlaenge/2
diagonale = (2**(1/2)) * seitenlaenge

def quadrat():
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        
def rechts_unten():
    down()
    quadrat()
    begin_fill()
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    left(90 + 45)
    fd(diagonale)
    left(45 + 90)
    end_fill()
    up()
    
def links_oben():
    down()
    quadrat()
    begin_fill()
    left(90)
    fd(seitenlaenge)
    right(90)
    fd(seitenlaenge)
    right(90 + 45)
    fd(diagonale)
    left(90 + 45)
    end_fill()
    up()
        
def links_unten():
    down()
    quadrat()
    begin_fill()
    left(90)
    fd(seitenlaenge)
    right(90 + 45)
    fd(diagonale)
    right(45 + 90)
    fd(seitenlaenge)
    end_fill()
    left(180)
    up()

def rechts_oben():
    down()
    quadrat()
    left(90)
    fd(seitenlaenge)
    begin_fill()
    right(90 + 45)
    fd(diagonale)
    left(90 + 45)
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    end_fill()
    left(90)
    up()
    

rechts_unten()
fd(seitenlaenge *2)
links_oben()
fd(seitenlaenge *2)
links_unten()
fd(seitenlaenge *2)
rechts_oben()

reset()

# Kreisbögen:
# Halbrunde Truchet-Tiles
def ring_links_oben():
    down()
    up()
    left(90)
    fd(halbseite)
    right(90)
    down()
    circle(halbseite, 90)
    up()
    right(90)
    fd(halbseite)
    right(90)
    fd(halbseite)
    right(90)
    down()
    circle(halbseite, 90)
    up()
    right(90)
    fd(halbseite)
    right(180)
    
def ring_rechts_oben():
    down()
    up()
    fd(halbseite)
    left(90)
    down()
    circle(halbseite, 90)
    up()
    right(90)
    fd(halbseite)
    right(90)
    fd(halbseite)
    right(90)
    down()
    circle(halbseite, 90)
    up()
    right(90)
    fd(halbseite)
    right(90)
    fd(seitenlaenge)
    right(180)
    
    
up()
goto(0,0)
ring_links_oben()
fd(seitenlaenge)
ring_rechts_oben()
reset()
    
print("2.2 Als Flächen")
# jetzt kacheln wir das mal auf ner Fläche :)
# und randomisieren dabei gleich mal welche Kachelform ausgewählt werden soll
# Diagonal-Tiles:
up()
goto(-400, -350)
down()
speed(0)
for j in range(7): # j entspricht "Höhe"
    for i in range(8): # i entspricht "Breite"
        # was ist in zufallszahl drin?
        zufallszahl = randint(1, 4) # Zahl zwischen 1 und 4
        # randint() gibt uns eine random Zahl zwischen 1 und 4
        # in HA7 Aufgabe 1.1 hab ich das schonmal erklärt :)
        # da wir das IN der for-Schleife machen, würfeln wir in jedem Durchlauf eine neue Zahl 
        
        # lasst euch zufallszahl printen:
        print(zufallszahl)
        if zufallszahl == 1: # wenn zufallszahl == 1, dann:
            rechts_oben()
        if zufallszahl == 2: # wenn zufallszahl == 2, dann:
            links_oben()
        if zufallszahl == 3: # wenn zufallszahl == 3, dann:
            rechts_unten()
        if zufallszahl == 4: # wenn zufallszahl == 4, dann:
            links_unten()
        fd(seitenlaenge)
    fd(-seitenlaenge*8) # Breite * Anzahl der Tiles zurückgehen 
    lt(90)
    fd(seitenlaenge)
    rt(90)
reset()

# Kreisbögen-Tiles:
   
up()
goto(-350, -350)
down()
pensize(10)
speed(0)
for j in range(8): # j entspricht "Höhe"
    for i in range(8): # i entspricht "Breite"
        # was ist in zufallszahl drin?
        zufallszahl = randint(1, 2) # Zahl zwischen 1 und 3
        # randint() gibt uns eine random Zahl zwischen 1 und 3
        # in HA7 Aufgabe 1.1 hab ich das schonmal erklärt :)
        # da wir das IN der for-Schleife machen, würfeln wir in jedem Durchlauf eine neue Zahl 
        
        # lasst euch zufallszahl printen:
        print(zufallszahl)
        if zufallszahl == 1: # wenn zufallszahl == 1, dann:
            ring_rechts_oben()
        if zufallszahl == 2: # wenn zufallszahl == 2, dann:
            ring_links_oben()
        fd(seitenlaenge)
    fd(-seitenlaenge*8) # Breite * Anzahl der Tiles zurückgehen 
    lt(90)
    fd(seitenlaenge)
    rt(90)
reset()    
    
print("Optional: Kopfrechenübungsprogramm (zum Einüben)")
print("1. Zahlen bekommmen")

schwierigkeit = 50 # das ist die obere Grenze für unsere random-Zahlen 
zahl1 = randint(0, schwierigkeit) # d.h wir würfeln eine Zahl von 0 bis 50
zahl2 = randint(0, schwierigkeit)

frage = "Was ist " + str(zahl1) + " + " + str(zahl2) + "?"
print(frage)
# wir konvertieren unsere Variablen in einen String damit wir String-Addition verwenden können
# zur Erinnerung: das Verbinden mit Plus geht nur, wenn unsere Elemente den selben Typp haben

print("2. Der Rest")

# wir rechnen das richtige au indem wir unsere beiden Zahlen addieren:
ergebnis = zahl1 + zahl2
print(ergebnis)

# unsere input()-Abfrage:
user_fragestellung = input(frage)
# oben haben wir unser Stringaddition in eine Variable gepackt, die wir jetzt hier abrufen

# die if-Abfrage, die feststellt ob der Input des Users mit dem richtigen Ergebnis übereinstimmt
# wir müsssen unseren User-Input noch zu nem Integer konvertieren
if int(user_fragestellung) == ergebnis:
    print("Das ist richtig!")
else:
    print("Das ist leider nicht richtig. Die richtige Antwort ist " + str(ergebnis))
