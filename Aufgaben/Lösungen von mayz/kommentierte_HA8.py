# Hausaufgabe 8, 2024-06-04 kommentiert von mayz
# die Themen heute sind:
# 1. Gemochte Zahlen
# 2. Tiles
# 3. Wiederholung
# Optional: Farbübergänge

from turtle import *
from random import randint

print("Hausaufgabe 8: Modulo, Tiles, Farbübergänge")

print("Gemochte Zahlen")
print("1.1 Nur Zahlen")
# das hier ist der Code aus Hausaufgabe 6:
# for i in range(30): # d.h wir prüfen die Zahlen von 0-29
    # if i % 7 == 0: # wenn diese durch 7 teilbar sind, dann:
        # print("Pfui! Bäh!")

# Piko gibt uns schon vor, dass wir die Zeile 'if i % 7 == 0:' weiterhin verwenden sollen
# zur Erinnerung: wenn diese Bedingung zutrifft, ist i durch 7 teilbar
# ein Vielfaches der Zahl ist natürlich durch die Zahl teilbar ohne Rest

# im ersten Schritt suchen wir nach Vielfachen der Zahl 7:
for i in range(30):
    if i % 7 == 0:
        print("Mag ich:", i)
        # wir printen den String und das i, das unsere if-Einschränkung erfüllt
        
# jetzt nochmal für Vielfache von 3:
for i in range(30): # denkt dran, range(30) nutzt die Zahlen von 0-29, 30 ist nicht inbegriffen
    if i % 3 == 0:
        print("Mag ich:", i)
        
# bisher printen wir nur, wenn unsere if-Einschränkung auf eine bestimmte Zahl zutrifft
# das erweitern wir jetzt durch einen print-Aufruf auch wenn "wir die Zahl nicht mögen" (also, die if-Einschränkung NICHT zutrifft)
for i in range(30):
    if i % 3 == 0: # für alle Zahlen, die durch 3 teilbar sind
        print("Mag ich:", i)
    else: # für alle Zahlen, die nicht durch 3 teilbar sind tu stattdessen:
        print("Mag ich nicht:", i)

print("1.2 Zahlen und Malen")
 
# wenn Python "die Zahl mag", soll ein roter Kreis gemalt werden, ansonsten ein schwarzer Kreis
# d.h falls i durch 3 teilbar ist, soll der Kreis rot sein
# andernfalls soll der Kreis schwarz sein

speed(0)
for i in range(30):
    if i % 3 == 0:
        farbe = "red" # Variable mit dem Inhalt "red"
        print("Mag ich:", i) # nicht notwendig für die Funktionalität des Programms, hilft aber bei der Veranschaulichung
    else:
        farbe = "black" # Variable mit dem Inhalt "black"
        print("Mag ich nicht", i) # nicht notwendig für die Funktionalität des Programms, hilft aber bei der Veranschaulichung
    color(farbe) # wir rufen die Variable auf
    # je nach dem ob der if- oder der else-Part der Einschränkung für i zutrifft ist der Inhalt der Variablen:
    # entweder "red", falls i durch 3 teilbar ist (if-Teil)
    # oder "black", falls i nicht durch 3 teilbar ist (else-Teil)
    circle(150)
    # left(10) 30 Mal 10 Grad geben 300 Grad also kein voller Kreis, weils hübscher is machen wir mal nen ganzen Kreis:
    left(360/30)
reset()

print("2. Tiles")
print("2.1 Mit Kreis")

# den Anfang des Programms gibt uns Piko vor:
def kreisquadrat(): # wir definieren unsere Funktion
    pensize(3)
    seitenlaenge = 50
    for i in range(4): # das Quadrat
        forward(seitenlaenge)
        left(90)
    # die Kreisgröße soll sich nur abhängig von der Seitenlänge verändern
    # d.h unsere Variable 'kreisgroesse' muss von der Variable 'seitenlaenge' abhängig sein:
    kreisgroesse = seitenlaenge/2 # der Radius des Kreises ist die Hälfte der Seitenlänge unseres Quadrats
    forward((seitenlaenge/2))  # damit der Kreis in der Mitte des Quadrats ist, müssen wir seitenlange/2 "nach rechts gehen" (deshalb das Minus)
    circle(kreisgroesse)  # der Kreis
    forward(-(seitenlaenge/2))  # damit die Turtle am Ende der Funktion genau da ist, wo sie am Anfang war, müssen wir noch zurück "nach links gehen" (deshalb das Minus)
          
kreisquadrat() # hier rufen wir die Funktion auf
reset()
print("2.2 Mit Strich")

# Der Satz des Pythagoras besagt allgemein:
# in einem rechtwinkligen Dreieck ist die Summe der Flächenquadrate der Kathetenquadrate gleich dem Flächeninhalt des Hypotenusenquadrats
# die Formel dazu dürfte bekannt sein:
# a² + b² = c²
# sucht euch Bilder oder ein Erklärvideo dazu, das ist deutlich anschaulicher als nur Worte!
# wir gehen von einem rechtwinkligen Dreieck aus (also ein Dreieck mit einem rechten Winkel)
# die Seiten des Dreiecks, die wir hier nur mit a,b und c benennen haben in der Mathematik die tollen Namen:
# a: Ankathete (die Seite, die an den gegebenen Winkel Alpha "anliegt" und NICHT die Hypothenuse ist)
# b: Gegenkathete (die Seite "gegenüber des Winkels Alpha")
# c: Hypotenuse (die längste Seite des Dreiecks, gegenüber des rechten Winkels)

# weil wir unsere Tiles quadratisch machen ist die Länge von Ankathete und Gegenkathete identisch
# d.h welche Seite a oder b ist, ist egal uns interessiert vor allem c
# die diagonale Linie, die wir malen wollen macht aus unserem Quadrat quasi zwei rechtwinklige Dreiecke
# die Diagonale ist die Hypotenuse (= c)

# weil wir ein Quadrat haben und a und b gleich sind können wir unsere Pythagoras-Gleichung ein bisschen umformen:
# a² + a² = d²
# wobei:
# a = seitenlaenge
# d = Diagonale
# das können wir nochmal umformen zu:
# 2 * a² = d²

# um jetzt herauszufinden, was d (also die Länge der Diagonalen) ist, müssen wir die Wurzel ziehen
# d = (zweite)Wurzel(2 * a²)
# bisschen Mathemagie *hust* Rechenregeln oder so und dieser Ausdruck lässt sich vereinfachen zu:
# d = a * (zweite)Wurzel(2) = (zweite)Wurzel(2) * a

# jetzt ist noch die Frage, wie wir in Python Wurzeln ausrechnen#
# Mathe ist manchmal ziemlich cool, deshalb gilt:
# (zweite)Wurzel(2) = 2 ** (1/2) ("zwei hoch einhalb")
# ansonsten rechnet es mit nem Taschenrechner aus oder schaut im Internet nach und rechnet mit dem Wert :)

# jetzt aber zum Code:
# die Seitenlänge = a = dreieckseite
# die Länge der Diagonalen drücken wir aus mit: (2**1/2)) * seitenlaenge

def diagonalquadrat():
    pensize(3)
    seitenlaenge = 50
    diagonale = (2**(1/2)) * seitenlaenge
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    left(90 + 45)
    fd(diagonale)
    fd(-diagonale)
    right(45)
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    left(90)
        
diagonalquadrat()
reset()

print("2.3 Eigene Tiles")

# definitiv optimierbarer Code!! aber es sieht so aus wie die Beispiele :D
# schreibt mich an wenn ihr Fragen habt, das war sehr viel trial-and-error
# Bsp 1:
def inspo1():
    stiftdicke = 3
    pensize(stiftdicke)
    seitenlaenge = 100
    diagonalhalbe = (2**(1/2)) * ((seitenlaenge/2) - stiftdicke)
    for i in range(4): # das Quadrat
        forward(seitenlaenge)
        left(90)
    for i in range(6):
        left(45)
        fd(diagonalhalbe)
        fd(-diagonalhalbe)
        right(45)
        fd(seitenlaenge/10)
    fd(seitenlaenge/2 - seitenlaenge/10)
    left(90)
    fd(seitenlaenge)
    left(90)
    for i in range(6):
        left(45)
        fd(diagonalhalbe)
        fd(-diagonalhalbe)
        right(45)
        fd(seitenlaenge/10)
    fd(seitenlaenge/2 - seitenlaenge/10)
    left(90)
    fd(seitenlaenge)
    left(90)

# Bsp 2:
def inspo2():
    stiftdicke = 3
    pensize(stiftdicke)
    seitenlaenge = 100
    halbeseite = seitenlaenge/2
    kreisgroesse = seitenlaenge/4
    for i in range(4): # das Quadrat
        forward(seitenlaenge)
        left(90)
    for i in range(4):
        fd(kreisgroesse)
        circle(kreisgroesse)
        fd(kreisgroesse + halbeseite)
        lt(90)

# Bsp 3:
def inspo3():
    stiftdicke = 3
    pensize(stiftdicke)
    seitenlaenge = 100
    halbeseite = seitenlaenge/2
    kreisgroesse = seitenlaenge/4
    for i in range(4): # das Quadrat
        forward(seitenlaenge)
        left(90)
    for n in range(3):
        up()
        fd(kreisgroesse)
        down()
        circle(kreisgroesse)
        up()
        fd(kreisgroesse)
        down()
        circle(kreisgroesse)
        up()
        fd(kreisgroesse)
        down()
        circle(kreisgroesse)
        up()
        fd(-(halbeseite + kreisgroesse))
        left(90)
        fd(kreisgroesse)
        right(90)
        down()
    up()
    right(90)
    fd(halbeseite + kreisgroesse)
    left(90)
    down()

# Bsp. 4:
def inspo4():
    stiftdicke = 3
    pensize(stiftdicke)
    seitenlaenge = 100
    halbeseite = seitenlaenge/2
    kreisgroesse = seitenlaenge/4
    for i in range(4):
        forward(seitenlaenge)
        left(90)
    up()
    left(90)
    fd(halbeseite)
    right(90)
    down()
    for i in range(4):
        circle(halbeseite, 90)
        left(180)
    left(90)
    fd(-halbeseite)
    right(90)

# Bsp 5:
def inspo5():
    stiftdicke = 3
    pensize(stiftdicke)
    seitenlaenge = 100
    halbeseite = seitenlaenge/2
    kreisgroesse = seitenlaenge/4
    for i in range(4):
        forward(seitenlaenge)
        left(90)
    for i in range(2):
        for i in range(3):
            fd(kreisgroesse)
            left(90)
            fd(seitenlaenge)
            fd(-seitenlaenge)
            right(90)
        fd(kreisgroesse)
        left(90)
    fd(seitenlaenge)
    left(90)
    fd(seitenlaenge)
    left(90)
    
speed(0)
up()
goto(-250, 0)
down()
inspo1()
up()
fd(110)
down()
inspo2()
up()
fd(110)
down()
inspo3()
up()
fd(110)
down()
inspo4()
up()
fd(110)
down()
inspo5()

reset()
print("2.4 Tile-Reihe")

speed(0)
seitenlaenge = 100
up()
goto(-300, 0) # damitit die Tiles schön ins Fenster passen
down()

for i in range(7): # unsere for-Schleife: 
    inspo1() # Aufruf für die oben definierte Funktion für eine Tile
    up()
    fd(seitenlaenge) # die Tiles sollen nahtlos aneinanderliegen 
    down()
reset()
    
print("3. Wiederholung")
print("3.1 Aufgabe auswürfeln")
# Piko gibt uns Wochen 1-6 und Aufgaben 1-4 vor, 
# wir würfeln aber von 1 bis 8 für alle bisherigen Wochen:
woche = randint(1, 8)
# dann würfeln wir von 1 bis 5 für die Anzahl der Aufgaben:
# Aufgabe 5 entspricht den optionalen Aufgaben
aufgabe = randint(1, 5)

print("3.2 Als Programm")

# wenn wir prüfen, welche Wochen 4 Aufgaben haben sehen wir:
# Wochen 1, 2, 3, 5, 8 haben 3 Aufgaben
# Wochen 4, 7 haben 2 Aufgaben
# Woche 6 hat 4 Aufgaben
# Wochen 5, 6, 7, 8 haben optionale Aufgaben

# wenn wir das nicht berücksichtigen sieht unser "generisches" Programm wie folgt aus:
print("Löse Aufgabe", aufgabe, "aus Woche", woche)

# wir können das aber auch bisschen komplizierter machen:
# wir lassen die optionalen Aufgaben mal weg, weil es darum geht zu wiederholen
woche_3A = [1, 2, 3, 5, 8] # alle Wochen mit 3 Aufgaben
woche_2A = [4, 7] # alle Wochen mit 2 Aufgaben
woche_4A = [6] # alle Wochen mit 4 Aufgaben


if woche in woche_3A: # wenn unsere Woche in der Liste ist
    aufgabe = randint(1, 3) # dann würfeln wir nur bis zur dritten Aufgabe
elif woche in woche_2A: # wenn unsere Woche in der Liste ist
    aufgabe = randint(1, 2) # dann würfeln wir nur bis zur zweiten Zahl
elif woche in woche_4A: # wenn unsere woche in der Liste ist
    aufgabe = randint(1, 4) # dann würfeln wir bis zur vierten Aufgabe
# unsere verbesserte Version:  
print("Löse Aufgabe", aufgabe, "aus Woche", woche)

print("Optional: Farbübergänge (Zum Einüben)")

print("1 Teppiche")
# Wenn ihr einen 10 Meter langen Gang mit fünf gleichlangen Teppichen auslegen wollt, wie lang muss dann jeder Teppich sein?
# der Ansatz ist ziemlich simpel:
# wir teilen 10 durch 5: 10/5 = 2
# jedes Stück muss 2 Meter lang sein
# dasselbe Prinzip gilt auch für 9 Meter Gesamtlänge und drei gleichgroße Stücke:
# wir teilen Gesamtlänge durch Stückzahl, also 9 durch 3 = 3
# Jedes Stück ist 3 Meter lang

# die Formel ist immer die Selbe: Ganglänge(Gesamtlänge) geteilt durch Anzahl der Stücke
# als Code sieht das dann so aus:
gang = 12 # das entspricht der Gesamtlänge
n_teppiche = 3 # das enstspricht der Anzahl Teppichstücke
# gesucht ist: die Länge der Teppichstücke
# wie oben erklärt finden wir diese indem wir 9/3 = 3 rechnen
# das übertragen wir jetzt auf unsere Variablen:
laenge_teppiche = gang / n_teppiche
# die Länge der Teppiche entspricht Ganglänge geteilt durch Anzahl der Teppiche
print("Die Länge der einzelnen Teppiche ist:", laenge_teppiche)

print("2 Zwischen 0 und 1")
# Wenn Ihr einen 9 Meter langen Gang mit drei gleichlangen Teppichen auslegen wollt, wie lang muss dann jeder Teppich sein?
# Wenn Ihr     von 0 bis 0.9         in  drei gleichgroßen Schritten gehen    wollt, wie groß muss dann jeder Schritt sein?

# der Gedanke ist genau der Selbe wie oben, nur ist es nicht ein 9 Meter langer Gang, sondern die "Gesamtgröße" von 0.0 bis 0.9
# dieser Wert errechnet sich durch die Differenz zwischen den beiden Zahlen
# diese errechnet sich aus der größeren Zahl minus die kleinere Zahl
# zwischen 0 und 0.9 liegt 0.9 weil 0.9 - 0 = 0.9
# die Anzahl der Schritte zwischen unseren Werten ist immernoch 3
# wir rechnen wieder Gesamtgröße geteilt durch Anzahl der Schritte:
# 0.9 / 3 = 0.3
# d.h die Schrittgröße ist 0.3

# Bsp. von 0 bis 0.5 in fünf gleichgroßen Schritten:
# unsere Gesamtgröße ist die Differenz zwischen 0 und 0.5, d.h 0.5
# die Schrittanzahl ist 5
# wir rechnen für die Schrittgröße: 0.5 / 5 = 0.1

# Bsp. von 0.5 bis 1 in 5 gleichgroßen Schritten:
# Differenz zwischen 1 und 0.5 = 0.5, d.h Gesamtgroesse = 0.5
# Schrittanzahl = 5
# wir rechnen für die Schrittgröße: 0.5 / 5 = 0.1

# Bsp von 0 bs 1 in fünf gleichgroßen Schritten:
# Differenz zwischen 0 und 1 = 1
# Schrittanzahl = 5
# wir rechnen für die Schrittgröße: 1 / 5 = 0.2

# Bsp: von 0.6 bis 0.8 in 4 gleichgroßen Schritten:
# Differenz zwischen 0.8 und 0.6 = 0.8 - 0.6 = 0.2
# Schrittanzahl = 4
# wir rechnen für die Schrittgröße: 0.2 / 4 = 0.05

# als Programm sieht das dann so aus:

start = 0.6
ende = 0.8
n_schritte = 4
# wir errechen die Gesamtgröße wie oben aus der Differenz zwischen Start und Ende
# dafür können wir einfach die Endzahl minus die Anfangszahl rechnen
gesamtgroesse = ende - start
# die Groesse der Schritte ist wieder Gesamtgroesse / Anzahl der Schritte:
schrittgroesse = gesamtgroesse / n_schritte
print("Um von", start, "bis zu", ende, "in", n_schritte, "Schritten zu kommen, braucht es die Schrittgröße", schrittgroesse)

print(" 3 Farbübergänge")
# 0.0,         0.1,         0.2,        0.3,         0.4,         0.5
# 0 * 0.1,    1 * 0.1,     2 * 0.1,    3 * 0.1,     4 * 0.1,     5 * 0.1

# die Anzahl der Schritte kommt in unsere range() Funktion
# in der ersten Wiederholung (i = 0), rechnen wir 0 * 0.1
# in der zweiten Wiederholung (i = 1), rechnen wir 1 * 0.1
# in der dritten Wiederholung (i = 2), rechnen wir 2 * 0.1
# und so weiter und so fort
# wir sehen, wir rechnen immer i * 0.1

for i in range(6):
    print(i * 0.1)
    
# 0.1 ist die Länge der Schritte zwischen 0 und 0.5 bei einer Schrittanzahl von 5
# weil wir 0.0 dabei haben wollten haben wir in der for-Schleife (Schrittanzahl + 1) in die range() Funktion gepackt

# jetzt wollen wir den Wert in einer Variablen speichern:
rot = 0.1 # das entspricht unsere Schrittgroesse
print("color(",rot,",0, 0)")

# als for-Schleife "ausbuchstabiert":
schrittanzahl = 5
for_schleife_anfang = 0
for_schleife_ende = 0.5
differenz = for_schleife_ende - for_schleife_anfang
rot = differenz / schrittanzahl

for i in range(schrittanzahl + 1):
    print("color(",rot * i, " ,0, 0)") # das brauchen wir nur wenn wir den Farbwert printen wollen
    
# als sinnvolles Beispiel mit Kreisen:
k_schritte = 5
k_anfang = 0
k_ende = 1
k_differenz = k_ende - k_anfang
k_rot = k_differenz / k_schritte

up()
goto(0,0)
down()

for i in range(k_schritte):
    pensize(10)
    color(k_rot * i, 0, 0)
    circle(100)
    left(360/k_schritte + 1)
reset()

print("4 Variable Variablen")
# jetzt noch mit Zufall!
from random import random
# wir importieren die Funktion random(), sie gibt uns einen zufälligen Wert zwischen 0 und 1
r_schritte = 5
r_anfang = 0
r_ende = random() # wir setzen wir das Ende zu der zufälligen Zahl
r_diff = r_ende - r_anfang
r_schrittgroesse = r_diff / r_schritte


up()
goto(0,0)
down()

for i in range(r_schritte):
    pensize(10)
    color(r_schrittgroesse * i, 0, 0)
    circle(100)
    up()
    fd(20)
    down()
   
reset()

print("5 start - zum Weiterdenken")
# rot = 0.6 + i * schrittgroesse
# der Anfang ist bei 0.6
# wir addieren dann die Schrittgroesse drauf

# wir können das auch umdrehen und die Schrittgroesse vom Ende abziehen:
# rot = 0.8 - i * schrittgroesse

m_schritte = 4
m_anfang = 0.6
m_ende = 0.8
m_differenz = m_ende - m_anfang
m_schrittgroesse = m_differenz / m_schritte

up()
goto(0,0)
down()

for i in range(m_schritte):
    pensize(10)
    color(m_ende - (i * m_schrittgroesse), 0, 0)
    circle(100)
    left(360/m_schritte)
reset()

# ich freue mich über Feedback falls euch diese Erklärungen helfen bzw. Verbesserungsvorschläge, falls Erklärungen unklar sind :)