# Hausaufgabe 4: 2024-05-07 kommentiert von mayz
# die Themen heute sind:
# 1. Zielscheibe
# 2. Sterne!

print("Hausaufgabe 4: Zielscheibe, Himmel, Sternfunktion: Tipps\n")

print("1. Zielscheibe")
print("1.1 einfarbige Zielscheibe\n")

# Gedanken, wie man eine Zielscheibe hinbekommt:

# wir brauchen zentrierte Kreise unterschiedlicher Größe
# die Kreise sollen nach außen hin immer größer werden

# das hier ist Code für einen einzelnen zentrierten Kreis (mit Radius 50 Pixel):

# up()
# forward(50)
# left(90)
# down()
# circle(50)

# für die Zielscheibe wollen wir, dass der zweite Kreis den doppelten Radius vom ersten Kreis hat
# und der dritte Kreis den dreifachen Radius
# und so weiter und so fort
# ohne es in eine for-Schleife zu packen, soll das hier passieren:

# up()
# forward(50) # hier steht quasi: forward(50 * 1)
# down()
# left(90)
# circle(50) # hier steht quasi: forward(50 * 1)
# up()
# goto(0, 0) # zurück zum Mittelpunkt
# 
# forward(50 * 2)
# left(90)
# down()
# circle(50 * 2)
# up()
# goto(0, 0)
#
# forward(50 * 3)
# left(90)
# down()
# circle(50 * 3)
# up()
# goto(0, 0)
#
# ... und so weiter und so fort

# wir sehen hier schon, dass der Code immer derselbe ist, nur in circle() ändert sich der Multiplikator
# das heißt im ersten Durchgang ist der Multiplikator 1,
# im zweiten Durchgang ist der Multiplikator 2,
# im Dritten ist er 3
# und so weiter und so fort

# wenn wir das ganze in eine for-Schleife packen, gibt uns range() an, wie viele Kreise wir haben wollen
# i ist der Index, der hochzählt, bis unsere Zielanzahl von Kreisen erreicht ist
# da Python von 0 anfängt zu zählen, ist der Multiplikator im ersten Durchgang *0
# der Kreis mit Radius 0 ist dann der Punkt in der Mitte

# der fertige Code sieht also so aus:
from turtle import *

pensize(20) 

# wir parametrisieren unsere Zielscheibe zusätzlich:
abstand = 50 # Abstand zwischen den Kreisen, der "weiße" Teil der Zielscheibe
anzahl_kreise = 6 # Anzahl der Ringe

for i in range(anzahl_kreise):
    forward(abstand * i)
    down()
    left(90)
    circle(abstand *i)
    up()
    goto(0,0)
    # wir machen uns das Leben einfacher indem wir die Turtle einfach zwingen wieder in die Mitte zu laufen (Koordinaten: 0, 0)
reset()


print("Zielscheibe mit Farbübergang")

# für den Farbübergang schauen wir uns zuerst nochmal den Code für den Farbverlauf in einem "Kreis" an:
# 
# from turtle import *
# pensize(20)
# for i in range(18):
#     print(i)
#     print(i/18)
#     color(0, 0, i/18)
#     forward(40)
#     left(20)
# 
# Erinnerung: RGB ist ein additives Farbmodel worin die Werte (r, g, b) jeweils für den "rot"-, "grün"- und "blau"-Werte stehen
# für den Übergang in blauer Farbe müssen wir also den dritten Wert verändern
# (0, 0, 0) ist schwarz
# (0, 0, 1) ist blau
# d.h für einen Farbübergang muss die dritte Zahl innerhalb der for-Schleife:
# 1) einen aufsteigenden Wert zwischen 0 und 1 für schwarz nach blau
# oder
# 2) einen absteigenden Wert zwischen 1 und 0 für blau nach schwarz erreichen
# 
# Mathe... juhu?
# keine Sorge, es ist relativ simpel:
# 
# 1) der aufsteigende Wert ist ziemlich einfach, wir teilen einfach unseren Index durch unsere range(x)
# also i/x für den dritten Farbwert
# im oberen Beispiel ist das i/18 (da range(18))
# je größer i wird, desto größer wird auch i/18,
# aber i/18 bleibt immer kleiner 1, und kleinergleich 0 weil
# für i=18: i/x = 17/18 = 0,94444 (weil Python bei Null anfängt zu zählen)
# und für i=0: i/x = 0/18 = 0
# probiert den Code aus und lasst euch i und i/18 printen, sonst schreibe ich hier ewig lange Listen, haha
# 
# 2) der absteigende Wert scheint schwierig, ist aber durch eine relativ einfachen Trick erreichbar:
# zur Erinnerung: i/x wird größer für größere x aber IMMER kleiner 1
# das machen wir uns zu nutze indem wir 1 nehmen und dann i/x davon abziehen: (1 - (i/x))
# auf unser Beispiel oben angewendet ist x=18 und i= 0 bis 17
# d.h für i=0 erhalten wir (1 - (0/17)) = (1 - 0) = 1
# für den RGB-Wert bedeutet das: für i=0: (r, g, b) = (0, 0, 1) das entspricht "blau"
# jetzt fangen wir also bei "blau" an, (1 - (i/x)) wird immer kleiner,
# also bewegen wir uns von "blau" nach schwarz (weil schwarz = (0, 0, 0))
# 
# probiert es aus und lasst euch jeweils wie im Beispiel oben i und i/x printen für schwarz zu blau
# und i und (1 - (i/x)) printen für blau nach schwarz

# fertiger Code: Zielscheibe mit Farbverlauf scharz innen und nach außen hin blau
# siehe 1) wir bekommen den Farbverlauf durch i/x
pensize(20)
for i in range(anzahl_kreise):
    print(i) # print Index i 
    print(i/anzahl_kreise) # print den dazugehörigen Wert i/x wobei x = anzahl_kreise
    color(0,0, i/anzahl_kreise)
    forward(abstand * i) # ansonsten is das der exakt gleiche Code wie für die normale Zielscheibe
    down()
    left(90)
    circle(abstand *i)
    up()
    goto(0,0)
    
reset()

print("1.3 Nachthimmel\n")

# Piko gibt hier ja schon den Hinweis, dass wir die Pensize dick machen sollen
# der Abstand ist jetzt so klein, dass sich die Striche überlappen
# das Parametrisieren von abstand und anzahl_kreise oben macht uns hier das Leben einfach:

# Nachthimmel mit scharz in der Mitte (quasi dasselbe wie 1.2 nur andere Werte):
speed(0)
pensize(10)
abstand = 5
anzahl_kreise = 40

for i in range(anzahl_kreise):
    print(i)
    print(i/anzahl_kreise)
    color(0,0, i/anzahl_kreise)
    forward(abstand * i)
    down()
    left(90)
    circle(abstand *i)
    up()
    goto(0,0)
    
reset()
print()

# jetzt machen wir, wie in der Aufgabe gefordert einen Farbverlauf von Blau nach Schwarz
# wir nutzen also 2) (1 - (i/x))
# Nachthimmel nach Aufgabenstellung mit blau in der Mitte
speed(0)
pensize(10)

abstand = 5
anzahl_kreise = 40

for i in range(anzahl_kreise):
    print(i)
    print(1-(i/anzahl_kreise))
    color(0,0, (1-(i/anzahl_kreise)))
    forward(abstand * i)
    down()
    left(90)
    circle(abstand *i)
    up()
    goto(0,0)
reset()

print("2. Sterne!\n")

# Piko gibt uns hier ja schon den Tipp, dass wir circle() und 180 Grad-Wenden brauchen
# wenn wir uns die Form des Sterns anschauen, verstehen wir auch schnell warum:
# der Stern ist der "negative Raum" zwischen vier Kreisen deren Ränder sich berühren (sie haben also den gleichen Radius)
# wir können uns den "positiven Raum" anschauen, wenn wir zuerst die vier Kreise malen:

# wir malen die vier Kreise, um sowohl den negativen, als auch den positiven Raum zu verstehen:
# das sieht dann so aus:

#from turtle import *

# up()
# goto(-50, 50) ## das goto() machen es einfacher sicherzustellen, dass sich die Kreise berühren
# down()
# circle(50)
# up()
# goto(50, 50)
# down()
# circle(50)
# up()
# goto(-50, -50)
# down()
# circle(50)
# up()
# goto(50, -50)
# down()
# circle(50)

# jetzt sehen wir unseren Stern "in der Mitte", umgeben von vier gleichgroßen Kreisen
# um nur den Stern zu bekommen, brauchen wir also jeweils die inneren Viertel der Kreise
# Erinnerung: wir können mit einer zweiten Zahl in circle() angeben, wie viel Grad des Kreises gemalt werden sollen
# ein ganzer Kreis hat 360 GArd, ein Viertel eines Kreises sind 90 Grad
# wir wollen also circle(x, 90) um nur ein Viertel eines Kreises mit Radius x zu malen
# jetzt kommen wir zu den 180-Grad-Wenden
# die sind der einfachste Weg uns nur auf die inneren Viertel-Kreise zu beschränken
# wir wollen:
# einen Viertel-Kreis
# "umdrehen" (also eine 180-Grad-Wende)
# wieder einen Viertel-Kreis malen
# wieder "umdrehen"
# und das insgesamt 4 Mal

# davor spezifizieren wir, welche Farbe der Stern haben soll
# dann kommt ein begin_fill() und an das Ende ein end_fill()

print("2.1 Programm")
# fertig sieht das dann so aus:
color("green")
begin_fill()
circle(100, 90)
left(180)
circle(100, 90)
left(180)
circle(100, 90)
left(180)
circle(100, 90)
end_fill()

reset() # kopiert den Code bitte ohne das reset(), das is nur dazu da alles schön hinteinander zu sehen 

# wir können das auch abkürzen und in eine for-Schleife packen:
# grüner Stern:
begin_fill()
for i in range(4):
    color("green")
    circle(100, 90)
    left(180)
end_fill()

reset()

# jetzt ist noch die Frage, wie wir den "sparkle"-Faktor erhöhen?
# bei zwei unserer Stern-Spitzen brauchen wir eine längere Linie
# das erreichen wir, indem wir nach dem ersten Viertel des Kreises
# einfach ein bisschen geradeaus gehen und dann wieder dieselbe Länge zurück
# das wiederholen wir dann nochmal beim dritten Kreis

# sonst ändert sich nichts

# als fertiger Code sieht das dann so aus:
# "sparkliger"-Stern:
pensize(5)
color("green")
begin_fill()
circle(100, 90)
forward(30) # hier gehen wir 30Pixel weiter und verlängern so die Linie
forward(-30) # und das wieder zurück damit der Stern symmetrisch ist
left(180)
circle(100, 90)
left(180)
circle(100, 90)
forward(30) # s.o
forward(-30)
left(180)
circle(100, 90)
end_fill()
reset()

# oder als for-Schleife:
begin_fill()
for i in range(2): # anstatt in 4 Viertel-Kreisen zu denken, halbieren wir jetzt den Stern
    # weil wir oben und unten die längere Linie haben wollen 
    # deshalb range(2) statt range(4) oben
    color("green")
    circle(100, 90)
    forward(30) # 
    forward(-30)
    left(180)
    circle(100, 90)
    left(180)
end_fill()
reset()


print("2.2 Funktion")

# nochmal zur Erinnerung:
# wir können unsere eigenen Funktionen definieren
# dazu nutzen wir: def Name_der_Funktion_():
# in dem eingerückten Codeblock folgen dann die Instruktionen für diese Funktion
# um die Funktion auszuführen müssen wir sie danach aufrufen
# nicht vergessen! Klammer-auf & Klammer-zu nach dem Namen der Funktion, sowohl beim definieren, als auch beim aufrufen

# wir definieren einen roten Stern:
def stern_rot():
    color("red")
    begin_fill()
    for i in range(4): # achtet auf die Einrückung
        circle(100, 90)
        left(180)
    end_fill()
    
# wir definieren einen gelben Stern:
def stern_gelb():
    color("yellow")
    begin_fill()
    for i in range(4):
        circle(100, 90)
        left(180)
    end_fill()

# jetzt haben wir Stern_rot() und Stern_gelb() definiert
# um sie auch auszuführen, müssen wir die Funktion "beim Namen rufen":

forward(-300)
stern_rot() # wir rufen jetzt Stern_rot() auf
# Resultat: wir malen den oben definierten roten Stern
forward(100)
stern_gelb() # jetzt rufen wir Stern_gelb() auf
# Resultat: wir malen den oben definierten gelben Stern

# hoffe das hilft :3