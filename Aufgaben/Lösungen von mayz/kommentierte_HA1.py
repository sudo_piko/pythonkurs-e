# Hausaufgabe 1: 2024-04-16 kommentiert von mayz
# Themen:
# 1. Python als Taschenrechner
# 2. Turtle

print("Hausaufgabe 1: Taschenrechner und Turtle\n")
# \n ist ein sogenannter "new line character", d.h wir fügen eine leere Zeile hinzu
# \n muss im String stehen

print("1: Python als Taschenrechner\n")

# wir können das in der Kommandozeile ohne das print-Statement machen
# für das Programm ist das print-Statement wichtig

print(456 + 789) # Addition
print(456 - 789) # Subtraktion
print(456 / 789) # Division
      
print(2*4) # integer input gibt einen integer zurück
# integer sind "ganze" Zahlen z.b 3, 8, 894281
print(2.0*4) # floating point input gibt floating point zurück
# floating point sind Dezimalzahlen z.b 1.8 oder 9,99999999
# im Folgenden abgekürzt als float
# Achtung! das Komma wird als . geschrieben

print(2*4) # output: 8
print(2 * 4) # output: 8
# Leerzeichen zwischen Zahl und Operator ändern nichts
# Operatoren sind z.b +, *, /, etc

print(456**4) # 456 hoch 4, (456*456*456*456)
print(456*456*456*456)

print(23//7) # sog. "floor division" / ganzzahliger Divisionsprozess
# rundet das Ergebnis auf den nächsten Integer ab
print(23/7) # normale Divsion, gibt immer einen float zurück

print(23%7) # sog. "modulo"
# gibt den Rest der Division


print("2: Turtle? Turtle!\n") # output sieht man nur mit Turtle Graphics
# kopiert einfach den Code und probiert es aus :)
from turtle import * # einmal alles aus der turtle importieren

forward(25) # bewegt die Turtle um (25) nach vorne
right(90) # rotiere um (90) Grad
forward(25)
right(90)
forward(25)
right(90)
forward(25)
# das war die "komplizierte" Art ein Viereck zu machen 

reset() # einmal alles weg bitte

# jetzt ein halbes Oktagon durch eine for-Schleife
for i in range(5): # wir machen den folgenden Code-Block, der eingerückt ist 5 Mal
    forward(100) # 100 nach vorne
    left(45) # um 45 Grad rotieren
    # results in half of an octagon
# i in range(1) führt den eingerücken Code-Block 1 Mal aus
# i in range(2) führt den eingerücken Code-Block 2 Mal aus
# i in range(3) führt den eingerücken Code-Block 3 Mal aus
# und so weiter und so fort

reset() # ein mal alles weg bitte

# jetzt das Quadrat durch die for-Schleife anstatt "kompliziert" wie oben:
for i in range(4): # i steht für Index und bedeutet einfach nur, dass wir den folgenden eingerückten Code-Block 4 Mal ausführen
    forward(80) # 80 nach Vorne
    right(90) # um 90 Grad rotieren (Innenwinkel im Quadrat)
