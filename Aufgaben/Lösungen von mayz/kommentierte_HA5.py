# Hausaufgabe 5 2024-05-14 kommentiert von mayz
# Die Themen heute sind:
# 1. Begrüßungsautomat
# 2. Mandalafarben
# 3. Taschenrechner
# 4. optional: Farbübergang weiterdenken

from turtle import *
goto(-400,0) # damit der Text in mein Turtle-Fenster passt
write("Hausaufgabe 4: Begrüßungsautomat, input für Farbe und Taschenrechner, Farbübergänge", True, "left", font =('Constantia', 15, 'normal'))
# Piko hat mich auf die write-Funktion der Turtle hingewiesen und omg es is sooo cool! :3
# zum Nachlesen: https://www.geeksforgeeks.org/turtle-write-function-in-python/
reset()

print("1. Begrüßungsautomat\n")


personenliste = ["James", "Jean-Luc", "Kathryn", "Benjamin", "Jonathan"]
# unsere Personenliste hat 5 Namen, d.h. unsere for-Schleife brauch range(5)
# als Begrüßung wollen wir den String "Hallo,"
# wir haben zwei Möglichkeiten ein Leerzeichen zu machen:
# 1) wir "addieren" einen String, der nur aus einem Leerzeichen besteht: " "
# 2) wir beenden unseren "Hallo,"-String nicht mit dem Komma, sondern machen noch ein Leerzeichen: "Hallo, "

# Einschub: Listenelemente aufrufen
# wie im Dienstagstreffen schon angesprochen, können wir auch einzelne Listenelemente aufrufen
# Python startet Zählen bei 0, d.h das Listenelement 0 ist der erste Name in der Liste
# Listenelement 1 ist der zweite Name der Liste
# und so weiter und so fort
# die Syntax  (der "Satzbau") in Python, um Listenelemente aufzurufen ist wie folgt:
# Name_der_Liste[Nummer des Listenelements]
# d.h um bspw. den ersten Namen aufzurufen brauchen wir:
# den Namen unsere Liste, eckige Klammern und die Nummer des Listenelements, also 0 (siehe Erklärung oben)
# "James" entspricht also personenliste[0]
# auf diese Art: Name_des_Tupels[Zahl des Tupelelements] geht das auch genauso für Tupel :)


print("1.1 Einfache Begrüßung")   
# das hier ist der Weg, wie wir es bisher gemacht haben:
# for <Schleifenvariable> in <Wertevorrat>
# wobei person die Schleifenvariable ist und personenliste der Wertevorrat
# wir brauchen daher weder i noch range()
# in der Variablen 'person' speichern wir einen String,
# "Hallo,", 'person' und "!" sind daher alle Strings
# d.h wir können in unsere Begrüßung Strings addieren mit Plus (+)

# wenn wir verschiedene Typen haben, z.b in der Variablen einen Integer und wir wollen das mit einem String zusammen printen können wir diese nicht einfach so addieren
# Beispiel:
# Anzahl_Apfel = 5
# entweder wir geben der Variablen einen neuen Typ sodass wir nur gleiche Typen haben: str(Anzahl_Apfel) um + zu benutzen
# print("Ich habe" + str(Anzahl_Apfel) + "Äpfel")
# hier fällt auf, dass wir kein Leerzeichen zwischen 'Ich habe', 5 und Äpfel bekommen
# mit + werden die Strings quasi "zusammengeklebt", ohne Leerzeichen
# entweder wir addieren dazwischen noch einzelne Leerzeichen:
# print("Ich habe" + str(Anzahl_Apfel) + " " + "Äpfel")
# oder wir nutzen ein Komma
# wenn wir ein Komma benutzen, können wir auch mehrer Typen kombinieren, die Notwendigkeit für str() entfällt:
# print("Ich habe", Anzahl_Apfel, "Äpfel")

# wenn wir das Gelernte jetzt auf die Aufgabe übertragen sehen wir:
# beim Plus wird kein Leerzeichen hinzugefügt
# um zwischen "Hallo," und dem Namen der Person trotzdem ein Leerzeichen zu bekommen gibt es mehrere Möglichkeiten:
# 1) wir enden unseren String mit einem Leerzeichen statt mit dem Komma: "Hallo, "
# 2) wir addieren einen Leerzeichen String: + " "
# 3) wir nutzen ein Komma anstelle von Plus 

for person in personenliste:
    print("Hallo,", person + "!")
    # valide Alternativen:
    # print("Hallo, " + person + "!")
    # print("Hallo," + " " + person + "!")
    
# jetzt versuchen wir das ganze nochmal mit der oben erklärten Methode für das Aufrufen von Listenelementen:
# das Prinzip ist immernoch: for <Schleifenvariable> in <Wertevorrat>:
# jetzt ist die Schleifenvariable allerdings i und der Wertevorrat ist range(5) (weil die Liste 5 Elemente hat)
# anstelle von 'person' rufen wir jetzt unser Listenelement auf
# wir wollen im ersten Durchgang (i = 0) das erste Listenelement (personenliste[0]),
# im zweiten Durchgang (i = 1) das zweite Listenelement (personenliste[1]) und so weiter
# die Zahl des Listenelement ist also jeweils dasselbe wie i
# d.h in der for-Schleife rufen wir personenliste[i] auf
for i in range(5):
    print("Hallo,", personenliste[i] + "!")
    
print("1.2 Begrüßung mit Zählen")

# wir benutzen die Vorarbeit aus 1.1
# wie wir bereits wissen können Variablen im Laufe von z.b einer for-Schleife mit einem neuen Wert belegt werden
# das machen wir uns zu Nutzen um von 1 bis 5 zu Zählen
# wir definieren eine Variable mit dem Namen 'zahl':
zahl = 1
# im ersten Durchlauf der print-Statements in dieser for-Schleife ist zahl = 1
for person in personenliste:
    print("Hallo,", person + "!")
    print("Das war Begrüßung Nummer", zahl)
    # jetzt belegen wir die Variable 'zahl' neu:
    # am Ende des ersten Durchgangs zählen wir eins hoch
    # d.h das nächste Mal wenn wir 'zahl' aufrufen, ist zahl = 2
    zahl = zahl + 1
    # falls euch das Probleme macht, lasst euch zusätzlich 'zahl' printen, das hilft beim Verständnis :)


# jetzt die Variante mit Aufrufen von Listenelementen:
# wir benutzen wieder die Vorarbeit aus 1.1
# im ersten Durchlauf ist i = 0, wir wollen aber 1
# im zweiten Durchlauf ist i = 1, wir wollen aber 2
# wir sehen hier schon das Muster, dass wir für die Nummer der Begrüßung (i + 1) brauchen:
for i in range(5):
    print("Hallo," + "" + personenliste[i] + "!")
    print("Dies war Begrüßung Numer", (i + 1))
   
    
print("1.3 Hallo- Eskalation")
# wir benutzen die Vorarbeit aus 1.2
# die Anzahl der Hallos entspricht der Nummer der Begrüßung
# das einzige, das wir noch brauchen ist, die Anzahl der "Hallo,"-Strings mit 'zahl' zu multiplizieren
zahl = 1
for person in personenliste:
    print("Hallo," * zahl, person + "!")
    print("Das war Begrüßung Nummer", zahl)
    zahl = zahl + 1

# Variante Listenelemente aufrufen:
# die Anzahl der Hallos entspricht der Nummer der Begrüßung
# auch hier brauchen wir nur den "Hallo, "-String mit 'zahl' zu multiplizieren
for i in range(len(personenliste)): # len() entspricht der Länge der Liste, also 5
    print(("Hallo, " * (i + 1)) + personenliste[i] + "!")
    print("Dies war Begrüßung Nummer", (i + 1))
    
    

print() # leere Zeile um den Überblick zu behalten

print("2. Mandalafarben")

# wenn wir in Python input() benutzen, muss dieser in einer Variablen gespeichert werden
# zwischen die Klammern schreiben wir den "Prompt", die "Forderung" an den Benutzer des Programms etwas einzugeben
# das Ziel unseres Programms ist es das "red" in color("red) zu ersetzen
# "red" ist ein String, unser input soll den selben Typ haben (sonst kommt ein TypeError)
# dazu spezifizieren wir vor dem input() zusätzlich das str() für String:
farbwort = str(input("Welche Farbe?:"))

pensize(5)
for i in range(18):
    # anstatt color("red") aufzurufen, wollen wir quasi color('Inhalt des Inputs') aufrufen
    # weil wir den Input als Variable gespeichert haben, müssen wir diese Variable aufrufen, um den Inhalt von input() zu bekommen
    color(farbwort)
    circle(100)
    left(20)
reset()

print()
print("3. Taschenrechner")

# wieder müssen wir innerhalb der Klammern in input() unsere Forderung stellen
# diesmal wollen wir Zahlen, keinen String
# dazu spezifizieren wir jetzt, dass wir einen Integer brauchen mit int()
# der Input muss auch wieder in Variablen gespeichert werden, damit wir diese später wieder aufrufen können
erste_zahl = int(input("was ist die erste Zahl?"))
zweite_zahl = int(input("was ist die zweite Zahl?"))

# wir rechnen jetzt die Summe der beiden Inputs aus:
print("Die Summe deiner Zahlen ist:", (erste_zahl + zweite_zahl))
print()


print("4. Optional: Farbverlauf weiterdenken")

print("4.1 Andere Farben")

# ich pack das alles mal auf eine "Seite":
# in Hausaufgabe 4 hab ich lang und breit erklärt, wie das mit dem Farbverlauf und der Mathematik dahinter aussieht
# lest das einfach nochmal durch, falls ihr Fragen habt, stellt sie gerne :)
# aber nochmal schreib ich das nicht auf :D

pensize(20)
schritte = 15
up()
goto(-400, 300)
down()
# schwarz nach grün
for i in range(schritte + 1):
    color(0, i/schritte, 0)
    forward(20)
    
up()
goto(-400, 250)
down()
# schwarz nach dunkelgrün
# 0.5 ist die Hälfte von 1
# wir können den Wert also einfach durch 2 teilen 
for i in range(schritte + 1):
    color(0, (i/schritte)/2, 0)
    forward(20)
    
up()
goto(-400, 200)
down()
# rot nach schwarz
for i in range(schritte + 1):
    color((1-i/schritte), 0, 0)
    forward(20)
    
up()
goto(-400, 150)
down()

# rot nach grün
for i in range(schritte + 1):
    color((1-i/schritte), i/schritte, 0)
    forward(20)
    
up()
goto(-400, 100)
down()

# von (1, 0, 1) nach (0, 1, 0)
# von pink nach grün
for i in range(schritte + 1):
    color((1-i/schritte), i/schritte, (1-i/schritte))
    forward(20)

up()
goto(-400, 50)
down()
# von (0.5, 0, 0) nach (1, 0, 0)
# von dunkelrot nach rot
for i in range(schritte + 1):
    print(0.5 + ((i/schritte)/2) )
    color(0.5 + ((i/schritte)/2), 0, 0)
    forward(20)

up()
goto(100, 100)
down()

print("4.2 Im Kreis")

pensize(20)
schritte = 15
# ein ganzer Kreis
for i in range(schritte + 1):
    color(i/schritte, 0, 0)
    forward(40)
    left(360/(schritte + 1))
up()
goto(-200, -300)
down()

pensize(20)
schritte = 15
# ein halber Kreis
# anstelle von 360 Grad nehmen wir nur die Hälfte: 180
for i in range(schritte + 1):
    color(i/schritte, 0, 0)
    forward(20)
    left(180/(schritte + 1))
    
up()
goto(200, -200)
down()

pensize(20)
schritte = 15
# zwei halbe Kreise hintereinander, sodass es wieder ein ganzer Kreis entsteht
# erst von schwarz nach rot
for i in range(schritte + 1):
    color(i/schritte, 0, 0)
    forward(20)
    right(180/(schritte + 1))
# und dann nochmal von rot nach schwarz    
for i in range(schritte + 1):
    color((1-i/schritte), 0, 0)
    forward(20)
    right(180/(schritte + 1))