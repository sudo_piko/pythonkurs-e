# Hausaufgabe 7: 2024-05-28 kommentiert von mayz
# die heutigen Themen sind:
# 1. Würfeln 
# 2. Punktekreis
# 3. Optional: Orientierung auf der Leinwand
print("Hausaufgabe 7: random: Würfeln und Punktekreise\n")
print("1. Würfeln")
print("1.1 Würfelspiel")

from random import * # wir importieren das random Modul

# ein Würfel hat 6 Seiten, also brauchen wir die Zahlen von 1 bis 6
# wir wollen außerdem keine Floats sondern einfach nur Integer
# die Funktionen dafür sind randrange(x, y) oder randint(x, y):

# randrange()
# die benötigten Elemente sind 2 Integer, man erhält eine (pseudo)-zufällige Zahl zwischen x und (y-1)
# man kann sich x als unter Grenze vorstellen und y als obere Grenze
# ähnlich wie bei range() ist daher die zweite Zahl (obere Grenze) selbst nicht im Zahlenbereich enthalten

# randint()
# die benötigten Elemente sind 2 Integer, man erhält eine (pseudo-)zufällige Zahl zwischen x und y
# anders als bei randrange() ist die zweite Zahl in dem Zahlenbereich enthalten

# wir definieren unsere Variable
gewuerfelte_Zahl = randint(1, 6) # wir erhalten mit randint() eine Zahl zwischen 1 und 6
# (unser Würfel soll nicht 0 würfeln, also fangen wir bei 1 an)
# wir lassen uns unser Ergebnis ausgeben:
print(gewuerfelte_Zahl)
# if-Abfrage um zu prüfen, ob unsere Zahl == 6 ist 
if gewuerfelte_Zahl == 6:
    print("Yay! Gewonnen!")

print("1.2 Wahrscheinlichkeit")

# die Lösung ist eigentlich schon in der Fragestellung "wenn ihr irgendwas über 90 würfelt" gegeben
# in Python "übersetzt" ist:
# hundertseitiger Würfel... entspricht 'randint(1, 100)
# wenn... entspricht 'if...(optional: else...)'
# irgendwas über 90... entspricht 'irgendeine_Zahl > 90'
# sodass ihr "Yay! Gewonnen!" bekommt... entspricht 'print("Yay! Gewonnen!")

irgendeine_Zahl = randint(1, 100) # unser hundertseitiger Würfel
# if- Abfrage um zu prüfen ob die Zahl größer als 90 ist
# wir lassen uns unser Ergebnis ausgeben:
print(irgendeine_Zahl)
if irgendeine_Zahl > 90: 
    print("Yay!! Gewonnen!")
# 10 von 100 Zahlen sind größer als 90: 91, 92, 93, 94, 95, 96, 97, 98, 99, 100
# d.h in 10/100 Fällen gewinne wir, das entspricht einer Gewinnchance von 10%

print("1.3 Immer wieder...")
# wir wollen wieder einen 6-seitigen Würfel, also brauchen wir  'randint(1, 6)'
# wir wollen 10 Mal würfeln, also brauchen wir 'for i in range(10):'

# for-Schleife für 10 Würfe:
for i in range(10):
    # weil wir in jedem neuen Versuch eine neue Zahl würfeln wolle, muss die randint()-Funktion IN die for-Schleife
    Wurf = randint(1, 6)
    # zur Überprüfung lassen wir uns die Zahlen ausgeben:
    print(Wurf)
    # wenn Wurf == 6, dann..:
    if Wurf == 6:
        print("Yayy! Gewonnen")

print("2. Punktekreise\n")
print("2.1 Gepunktete Linie")

from turtle import *

# selbes Prinzip wie die gestrichelte Linie aus HA3 Aufgabe 1.2
# schaut euch die Erklärung da an, falls euch das Probleme macht
for i in range(20):
    dot()
    up()
    fd(20)
    down()
reset()
print("2.2 Manche Punkte")

# wir kombinieren jetzt das Programm aus Aufgabe 1.3 mit der gepunkteten Linie
# das 1.3 Programm können wir vom Prinzip her übernehmen
up()
goto(-450, 0) # damit wir genug Platz für unsere Linie haben
down()
# for-Schleife für 20 potentielle dots:
for i in range(40):
    color("red") # nur um die beiden Aufgaben besser unterscheiden zu können
    # weil wir in jedem neuen Versuch eine neue Zahl würfeln wolle, muss die randint()-Funktion IN die for-Schleife
    vllt_dot = randint(1, 6)
    # zur Überprüfung lassen wir uns die Zahlen ausgeben:
    print(vllt_dot)
    # bewegen müssen wir uns unabhängig davon, ob wir eine 6 würfeln
    up()
    fd(20)
    down()
    # wenn Wurf == 6, dann..:
    if vllt_dot == 6: # hier müssen wir auch noch unsern dot() unterkriegen
        # Einrückung beachten
        print("Huiiiiiiiiii") 
        dot()
reset()        
print("2.3 Kreis")
up()
goto(0,0) # damit wir wieder in der Mitte sind
down()
# wir können den Code aus 2.2 kopieren und um left(10) am Ende der for-Schleife ergänzen
for i in range(40):
    color("blue") # nur um die beiden Aufgaben besser unterscheiden zu können
    # weil wir in jedem neuen Versuch eine neue Zahl würfeln wolle, muss die randint()-Funktion IN die for-Schleife
    vllt_dot = randint(1, 6)
    # zur Überprüfung lassen wir uns die Zahlen ausgeben:
    print(vllt_dot)
    # bewegen müssen wir uns unabhängig davon, ob wir eine 6 würfeln
    up()
    fd(20)
    down()
    # wenn Wurf == 6, dann..:
    if vllt_dot == 6: # hier müssen wir auch noch unsern dot() unterkriegen
        # Einrückung beachten
        print("Huiiiiiiiiii") 
        dot()
    # Achtung bei der Einrückung:
    #left(10)
    # wenn wir uns nur genau ein Mal im Kreis drehen wollen, dann brauchen wir nicht 10 Grad
    # sondern (360/40) Grad weil wir range(40) ausführen:
    left(360/40)
reset()

print("2.4 Funktion und Mandala")
# jetzt packen wir das noch schön in eine Funktion:
anzahl_dots = 100
def punktekreis():
    for i in range(anzahl_dots):
        color("purple")
        zufall_dot = randint(1, 6)
        up()
        fd(10)
        down()
        if zufall_dot == 6:
            dot()
        left(360/anzahl_dots)
punktekreis()
reset()
# wenn ihr nur den Punktekreis haben wollt, ohne den Zufall, dann müsst ihr das if-Statement weglassen
# achtet dann auf die korrekte Einrückung von dot()

print("Optional: Sich auf der Leinwand orientieren (zum Einüben)")

# macht euch davor nochmal 3 Dinge klar:
# 1. viele Wege führen zum Ergebnis
# der Anspruch an das Programm ist erst mal nur so gut wie möglich zu funktionieren, egal wie kompliziert oder ohne for-Schleifen etc
# die Optimierung kommt bei Bedarf danach

# 2. das Programm, das ihr schreibt, gibt dem Computer Anweisungen
# grade wenn es um sowas wie die Turtle geht, schreibt ihr eigentlich nur eine Liste von Anweisungen für den Computer etwas zu malen
# also sollte der erste Gedanke sein: wie würde ICH das malen mit Stift und Papier
# z.b für das Peace-Zeichen: "ich würde einen Kreis malen", dann "einen Strich in der Hälfte des Kreises", dann ...
# diese Schritt-für-Schritt Anleitung könnt ihr dann in Python-Code "übersetzen"
# "einen Kreis malen" entspricht "circle()" und so weiter

# 3. schaut im Zweifelsfall nach, was die Funktionen bedeuten z.b wenn ihr vergessen habr wie man nur einen Halbkreis macht
# niemand erwartet von euch, jede Funktion und ihre korrekte Anwendung auswendig zu wissen
# Suchmaschinen sind eure Freunde (auch wenn es manchmal frustrierend sein kann)
# falls ihr gar nicht mehr klar kommt, stellt Fragen in den Chat oder direkt an Piko (ich helfe auch gerne falls ich kann :))
# wie Piko schon erwähnt hat kann ich auch das Fediverse empfehlen

# zum Peace-Zeichen gibt es hier schon mindestens 2 Möglichkeiten:
# ein Peace-Zeichen so wie ich (mayz) es angehen würde:
pensize(10)
kreis_groesse = 100 # Parametrisierung damit wir die Größe des Peace-Zeichens schnell anpassen können
# das Peace-Zeichen
def peace_zeichen():
    color("green")
    circle(kreis_groesse)
    circle(kreis_groesse, 180) # wir wollen an den oberen "Rand" des Kreises (also nur ein Halbkreis)
    left(90)
    # das (erste) Argument von circle() ist der Radius, wenn wir einen Strich durch den ganzen Kreis wollen
    # dann brauchen wir den Durchmesser: 2 * Radius
    fd(2 * kreis_groesse) 
    fd(-kreis_groesse)
    # stellt euch einen Kuchen (oder Pizza!) vor, den ihr in 8 Teile teilt
    # da dürfte euch schon auffallen, dass ihr mit dieser Aufteilung leicht die Winkel für das Peace-Zeichen bekommt
    # ein Achtel von einem Kreis hat 45 Grad
    right(45) 
    fd(kreis_groesse)
    fd(-kreis_groesse)
    left(90)
    fd(kreis_groesse)
    
peace_zeichen()
reset()
# in einem Video oder in einem der Dienstagstreffen hat Piko mal diesen Code gezeigt
# so würde  Piko ein Peace-Zeichen machen:
def peacezeichen():
    radius = 100
    pensize(radius / 5)
    left(90)
    forward(-1 * radius)
    # resultat: radius nach unten laufen 

    #kreis
    right(90)
    circle(radius)
    left(90)

    # striche
    fd(radius * 2)
    lt(90)
    circle(radius, 135)
    lt(90)
    fd(radius)
    rt(90)
    fd(radius)

    # zurück zum ausgangspunkt
    fd(-1 * radius)
    lt(45)
peacezeichen()
reset()
# ihr könnt hier schon sehen, dass Piko und ich einen leicht unterschiedlichen Ansatz für die Orientierung haben
# es geht nicht darum es genau gleich zu machen wie Piko oder wie ich
# ehrlich, schreibt den Code so wie IHR versuchen würdet ein Bild zu malen bzw jemanden dazu anzuleiten
# es gibt kein "richtig" oder "falsch"
# falls ihr dann am Ende noch unzufrieden seid, könnt ihr überlegen, was man optimieren kann

# eine Schneeflocke
# Tipp: sucht eucht ein Bild von einer Schneeflocke im Internet und überlegt euch wie ihr das nachmalen würdet
# Tipp 2: wenn euch Symmetrien auffallen könnt ihr schonmal überlegen, inwiefern man das ganze in for-Schleifen packen kann

# alsooo, ich hab das jetzt mal ein bisschen aufgeschlüsselt in die einzelnen Schritte:

# Schneeflocke (bisschen langatmig)
pensize(20)
# der Code sieht gut aus für flocken_laenge von ca. 150 - 500
# der Grund dafür ist, dass die 40 ziemlich random von mir bei pensize(20) & flocken_laenge = 300 gewählt wurde
# idealerweise sollte diese Zahl eine Abhängigkeit von der flocken_laenge haben, damit es immer schön skaliert
flocken_laenge = 300

left(90)
for i in range(6):
    fd(flocken_laenge)
    fd(-flocken_laenge)
    left(360/6)

for i in range(6):
    fd(4/5 * flocken_laenge)
    left(45)
    fd(40)
    fd(-40)
    right(90)
    fd(40)
    fd(-40)
    left(45)
    fd(-(4/5 * flocken_laenge))
    left(360/6)
    
for i in range(6):
    fd(3/5 * flocken_laenge)
    left(45)
    fd(40 * 2)
    fd(-40 * 2)
    right(90)
    fd(40 * 2)
    fd(-40 * 2)
    left(45)
    fd(-(3/5 * flocken_laenge))
    left(360/6)
    
for i in range(6):
    fd(2/5 * flocken_laenge)
    left(45)
    fd(40)
    fd(-40)
    right(90)
    fd(40)
    fd(-40)
    left(45)
    fd(-(2/5 * flocken_laenge))
    left(360/6)

for i in range(6):
    fd(1/8 * flocken_laenge)
    left(45)
    fd(40 * 2)
    fd(-40 * 2)
    right(90)
    fd(40 * 2)
    fd(-40 * 2)
    left(45)
    fd(-(1/8 * flocken_laenge))
    left(360/6)
reset()

# diesen Code können wir optimieren :D (zumindest ein bisschen)
# skalierbare Schneeflocke:
pensize(5)
flocken_laenge = 50
# die Länge der Kristalle ist jetzt nicht mehr einfach immer 40, sondern abhängig von flocken_laenge
kristall = 1/8 * flocken_laenge

left(90)
for i in range(6):
    fd(flocken_laenge)
    fd(-(1/5) * flocken_laenge)
    right(45)
    fd(kristall)
    fd(-kristall)
    left(90)
    fd(kristall)
    fd(-kristall)
    right(45)
    
    fd(-(1/5) * flocken_laenge)
    right(45)
    fd(2 * kristall)
    fd(-2 * kristall)
    left(90)
    fd(2 * kristall)
    fd(-2 *  kristall)
    right(45)
    
    fd(-(1/5) * flocken_laenge)
    right(45)
    fd(kristall)
    fd(-kristall)
    left(90)
    fd(kristall)
    fd(-kristall)
    right(45)
    
    fd(-(2/5) * flocken_laenge)
    fd(1/8 * flocken_laenge)
    left(45)
    fd(2 * kristall)
    fd(-2 * kristall)
    right(90)
    fd(2 * kristall)
    fd(-2 * kristall)
    left(45)
    fd(-(1/8) * flocken_laenge)
    
    left(360/6)
reset()
    
# eine Sonne auf hellblauem Hintergrund:
bgcolor(0.3, 0.8, 0.95) # hellblau

pensize(8)
color(1, 0.9, 0.15) # gelb
anzahl_strahlen = 30
laenge_strahlen = 300

up()
forward(50)
left(90)
down()
begin_fill()
circle(50)
end_fill()
right(90)
fd(-50)

for i in range(anzahl_strahlen):
    fd(laenge_strahlen)
    fd(-laenge_strahlen)
    left(360/anzahl_strahlen)
    
reset()
# tobt euch aus :)

# an der Blume arbeite ich noch, dafür hab ich grade keine Geduld
# viel Spaß bei euren eigenen Projekten :3