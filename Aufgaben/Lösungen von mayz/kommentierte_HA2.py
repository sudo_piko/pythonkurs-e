# Hausaufgabe 2: 2024-04-23 kommentiert von mayz
# 1. Mandalas
# 2. Drei Kreise & PRIMM
# 3. Variablenvariation

print("Hausaufgabe Nr. 2: Mandalas,PRIMM, Variablen\n")

print("1: Mandalas")

from turtle import *

speed(0) # schnellstmöglich
pensize(2) # "Stift"-größe 
for i in range(30):
    circle(100)  
    penup() # "den Stift vom Papier heben"
    forward(10)
    pendown() # "den Stift wieder auf das Papier setzen"
# kein right() oder left(), wir rotieren die Kreise also nicht, wir bewegen uns nur nach vorne
# forward(20) würde die Distanz zwischen den Kreisen auf 20 erhöhen
# forward(i*10) erhöht die Distanz zwischen den Kreisen zu 'index * 10'
# circle(200) macht alle Kreise größer 
# circle(i*100) variiert die Kreisgröße abhängig vom Index i
reset()

print("2. Three Circles\n")

print ("2.1 PRIMM")

pensize(20)

penup() 
forward(-200) # negative Zahl entspricht "rückwärts" laufen / nach links laufen
pendown()
color("blue")
circle(80)
# Resultat: ein blauer Kreis um (200) nach links verschoben
penup()
forward(200) 
pendown()
color("blue")
circle(80)
# Resultat: ein blauer Kreis in der Mitte
penup()
forward(200)
pendown()
color("blue")
circle(80)
# Resultat: ein blauer Kreis (200) nach rechts verschoben
# insgesamt: drei blaue Kreise 
reset()

print()

print("2.2 Variablenstuff\n")

# from turtle import *
# folgender Code ist eine Variation von 2.1  für 3 rote Kreise

farbe = "red" # Variable für color(), sie ersetzt das "red" in der Klammer
entfernung = 200 # Variable für forward(), sie ersetzt 200 in der Klammer

pensize(20)

penup()
forward(-entfernung) # entspricht forward(-200) aufgrund der oben definierten Variable
pendown()
color(farbe) # entspricht color("red") aufgrund der oben definierten Variablen
circle(80)

penup()
forward(entfernung)
pendown()
color(farbe)
circle(80)

penup()
forward(entfernung)
pendown()
color(farbe)
circle(80)

reset()

print()

print("3. Variablenvariation\n")

# dieser Code resultiert in einer Spirale
# es fehlt eigentlich: from turtle import *
laenge = 20

for i in range(10):
    forward(laenge) 
    left(55)
    
    laenge = laenge + 10

# Python fängt Zählen bei 0 an
# i=0 : forward(20), left(55), laenge ist jetzt 30
# i=1 : forward(30), left(55), laenge ist jetzt 40
# und so weiter und so fort

reset() # cleanup

# das hier ist ein Stern nach dem selben Prinzip wie oben
# die Veränderung des Winkels durch left() ergibt jetzt einen Stern statt der Spirale

laenge = 40

for i in range(80):
    forward(laenge) 
    left(160)
    
    laenge = laenge + 10