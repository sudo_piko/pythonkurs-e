# Aufgaben vom 10.07.2024 – Woche 13


1. Schaut Euch die Videos zur Woche 13 an: https://tube.tchncs.de/c/pykurs_e/
2. Macht das Quiz: https://pythonkurs.ithea.de/
3. Versucht Euch hier an den Aufgaben!


## 1 – Wörterfinden

### 1.1 Holt sie raus!
Zugegeben, die Aufgabe ist etwas albern, aber Indizierung und Slicing ist ein wichtiges Konzept und das hier ist 
seeeeeeehr anschaulich...

Findet die Wörter und holt sie mit Indizierung heraus:
zB: Trottel => rot => "Trottel"[1:4]

- Reblaus
- Kugelblitz
- Schund
- Dachschindel
- Scherz
- Baugerüst
- Kaugummiautomat
- Homeowner
- Scheinselbstständigkeit

### 1.2 Findet sie und holt sie raus...
In den folgenden Wörtern sind Wörter versteckt, aber Ihr braucht dann auch die Schrittweite, also wort[5:11:3] oder so!
```
überzüchten 
angepfiffen
sinneszelle
nachbohren 
drehbeginn 
losrattern 
backsteinern
asienreise
```

<details> 
  <summary>Die gesuchten Wörter (Spoiler)</summary>
überzüchten -> brühe //
angepfiffen -> neffe //
sinneszelle -> insel //
nachbohren -> ahorn //
drehbeginn -> rhein //
losrattern -> orten //
backsteinern -> aktien //
asienreise -> serie

</details>


## 2 – Listen korrigieren

Hier sind Listen, die irgendwie falsch sind. Findet den Fehler (oder denkt Euch selbst einen aus :D) und korrigiert 
ihn so:
```python
rgb_farben = ["rot", "grün", "schwarz"]
rgb_farben[2] = "blau"

print(rgb_farben)
```
Mit Indexing (und eventuell `del`):
```python
wochentage = ["Montag", "Dienstag", "Mittwoch", "Schokoladentag", "Freitag", "Samstag", "Sonntag"]
planeten = ["Merkur", "Aphrodite", "Erde", "Mars", "Jupiter", "Saturn", "Neptun", "Pluto"]
phasen_des_debugging = ["So, jetzt noch schnell eine Klammer zu und dann ist es fertig! Ausführen!",
                        "Oh. Warum funktioniert das denn nicht?",
                        "Wie konnte das jemals funktionieren?",
                        "Ich habe das Gefühl, dass ich in einem Labyrinth aus Code gefangen bin.",
                        "Ich frage mich, ob ich jemals wieder das Licht des Tages sehen werde.",
                        "Ich werde meine Seele verkaufen, nur um diesen Fehler zu finden.",
                        "Ich habe das Gefühl, dass ich in einem endlosen Loop feststecke.",
                        "Ich bin sicher, dass dieser Code mein Untergang sein wird."]
```

Hier könnt Ihr es mit Slicing, nicht nur mit Indexing machen:

```python
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter", "Wartezeit", "Wartezeit", "Wartezeit", "Wartezeit", "Wartezeit"]
monate = ["Januar", "Februar", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]
zahlwoerter = ["eins", "zwei", "drei", "sechs", "sieben", "acht"]
```

<details> 
  <summary>Tipps </summary>
   Orientiert Euch am Beispiel, das ich vorgegeben habe. Wenn Ihr stecken bleibt, schlagt im Internet nach! Formuliert auf Deutsch, was Ihr machen wollt, übersetzt es auf Englisch und gebt es in eine Suchmaschine ein. Schaut Euch dort die Beispiele an.
</details>


## 3 – Methoden

Die Methoden könnt Ihr hier nachschlagen:  
https://www.w3schools.com/python/python_ref_string.asp

### 3.1 Strings manipulieren

Hier ist eine Tabelle. Was links steht, sollt Ihr verwenden, um das, was rechts steht draus zu machen.


| Ausgangs-String                                        | Ziel                                                                          |
|--------------------------------------------------------|-------------------------------------------------------------------------------|
| "firlefanz"                                            | "FIRLEFANZ"                                                                   |
| "POSSIERLICH"                                          | "possierlich"                                                                 |
| "horzallorliobst"                                      | "herzallerliebst"                                                             |
| "Buch Fisch Garnele Garneele Garnöle Hase Dose Fichte" | ['Buch', 'Fisch', 'Garnele', 'Garneele', 'Garnöle', 'Hase', 'Dose', 'Fichte'] |
| "   &nbsp; &nbsp; &nbsp;  Kringel &nbsp; &nbsp;    "   | "Kringel"                                                                     |
| "pAmpELMUse"                                           | "PaMPelmuSE"                                                                  |


Also zB:
```python
wort = "firlefanz"
neues_wort = wort.upper()
print(neues_wort)  # => FIRLEFANZ
```

### 3.2 Strings auswerten

:zap: Lasst Python für Euch rausfinden, wie viele "e" in "Erstsemesterbestenfeste" sind – auch dafür gibt es eine Methode.

:zap: Schaut Euch folgenden Code an:

```python
wort = "n8twächter"
if wort.isalpha():
    print("Das ist ein Wort.")
else:
    print("Das ist kein gewöhnliches Wort.")
```

:zap: Beschreibt, was `isalpha()` da tut! Formuliert die Bedingung in Umgangssprache: "Wenn ..."

:zap: Sucht die richtige Methode, damit folgender Schnipsel passt:
```python
wort = "zwille"  # Oder "2lle" oder "ZwillE"
if wort.XXXXX():
    print("Alle Buchstaben in diesem Wort sind Kleinbuchstaben.")
else:
    print("In diesem Wort gibt es auch Großbuchstaben.")
```


## Optional: Automatisches Wörterfinden (zum Weiterdenken)

In Aufgabe 1 habe ich Euch Wörter gegeben, die Ihr durch Überlegen finden solltet und dann mit Indizierung anzeigen solltet – Euer Job war (neben dem Finden der Wörter), die richtigen Zahlen zu finden, also zB 5, 11 und 3 im Beispiel von Aufgabe 1.2. Wenn Ihr aber ein Ausgangswort und ein Lösungswort habt, könntet Ihr dann Python diese Zahlen finden lassen? Stellt Euch vor, Ihr bekommt eine Liste mit tausend solcher Ausgangswort-Lösungswort-Paare, da würdet Ihr die Zahlen nicht per Hand suchen wollen... 

Vorsicht, diese Aufgabe ist sehr knifflig! Tut bei Eurer Lösung erstmal so, als würde bei jedem Wort jeder Buchstabe nur einmal vorkommen. Überlegt genau, wie Ihr das per Hand macht.


Ihr braucht dafür wahrscheinlich die Funktion `len()`, die die Länge eines Strings (oder auch einer Liste) ausgibt. range(anfang, ende) könnte in dem Kontext auch recht nützlich sein.

