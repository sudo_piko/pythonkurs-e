# Aufgaben vom 07.05.2024 – Woche 04


1. Schaut Euch die Videos zur Woche 4 an: https://diode.zone/c/pykurs_e/
2. Macht das Quiz: https://pythonkurs.ithea.de/
3. Versucht Euch hier an den Aufgaben!


# 0 - Lesestoff

Zwei Wikipedia-Artikel und ein Video für Leute, die *noch* mehr über Farben wissen wollen:

Wikipedia zu Farbmischungen und RGB:  
https://de.wikipedia.org/wiki/Farbmischung#Additive_Farbmischung  
https://de.wikipedia.org/wiki/Additive_Farbmischung  

Vortragsvideo von der Gulasch-Programmiernacht 2022:  
https://media.ccc.de/v/gpn20-92-was-ist-eigentlich-farbe-

## 1 – Zielscheibe

### 1.1 Einfarbig
Schreibt ein Programm, das mit einer for-Schleife eine Zielscheibe malt:  
![W4_Zielscheibe_schwarz.png](..%2FAbbildungen%2FW4_Zielscheibe_schwarz.png)

<details> 
  <summary>Tipps </summary>
Der Radius der Kreise sollte abhängig von der Schleifenvariable sein. Nehmt dafür den ganz klassischen Schleifenkopf `for i in range(10)` (oder statt 10 auch gern eine andere Zahl). Der erste Kreis könnte zB den Radius 0 haben, der zweite 10, der dritte 20, usw. Um Euch besser zu orientieren, lasst Euch `i` in jeder Schleife ausgeben, so wie ich es in den Videos mache: `print(i)`, oder macht eine Tabelle!    
</details>

<details><summary>Tipps: Meine Kreise sehen eher aus wie ein Tunnel...</summary>
Um eine echte Zielscheibe zu bekommen, müssen die Mittelpunkte der Kreise aufeinander liegen, nicht die Anfangspunkte der Turtle-Linie für die einzelnen Kreise... Das heißt, geht am Anfang jedes for-Schleifendurchgangs mit `penup()` aus dem Mittelpunkt bis zur Kreislinie und dann erst (mit `pendown()` den Kreis entlang. Und am Ende des Schleifendurchgangs muss die Turtle wieder zurück in die Mitte gehen, damit sie am Anfang des nächsten Schleifendurchgangs schon dort ist... Wenn Ihr verwirrt seid, dann stellt Euch vor, Ihr seid die Turtle, und bekommt die Kommandos ("100 Schritte vorwärts! 90 Grad nach links! Einen Kreis mit 100 Schritten Radius!"). 
</details>


### 1.2 Farbübergang

Macht einen Farbübergang dazu!   
![W4_Zielscheibe.png](..%2FAbbildungen%2FW4_Zielscheibe.png)

<details>
  <summary>Tipps </summary>
Der Blau-Wert muss auch von i abhängig sein. Macht Euch auch da eine Tabelle! Die for-Schleife wird am Ende sehr ähnlich zu der sein, die Piko im Video baut.
</details>

### 1.3 Nachthimmel

- Macht die `pensize()` so dick, dass die einzelnen Linien der Zielscheibe einander berühren. 
- Kehrt die Richtung des Farbübergangs um: innen soll blau sein, außen schwarz. Helft Euch mit einer Tabelle, um zu verstehen, wie Ihr den Blauwert ausrechnen könnt.

<details>
  <summary>Tipps: Ich starre seit fünf Minuten auf meine Tabelle und komm nicht drauf, wie ich das ausrechnen lassen kann... </summary>
    Sagen wir, wir zählen i bis 10 hoch. Wenn i=0, dann soll der Blauwert 1 sein. Wenn i=1, dann soll er 0.9 sein, i=2 soll er 0.8 sein, und so weiter. Wie können wir aus der 2 eine 0.8 machen? 0.8 ist ja `1-0.2` . 0.2 ist `2/10`. Und die 2 ist unser i. Aaaaalso: `1 - (i/10)`. Und die 10 kriegen wir, wenn wir gucken, was in `range()` drin steht. Wenn wir uns das aus größerer Entfernung nochmal anschauen, dann fällt etwas auf: das ist einfach 1 - "das, was wir vorher hatten". Um den Farbübergang umzukehren, müssen wir also einfach `1 - ` davor schreiben!
</details>

Wenn Euch jetzt noch die einzelnen "Bahnen" zu deutlich sichtbar sind, dann macht sie dünner und nehmt mehr davon, dann wird der Übergang ganz gleichmäßig:  
![W4_Himmel.png](..%2FAbbildungen%2FW4_Himmel.png)

## 2 – Sterne!

### 2.1 Programm
Jetzt brauchen wir noch Sterne!

Macht ein Programm, das einen Stern malt. Füllt ihn mit `begin_fill()` und `end_fill()` (Schaut gerne nochmal in [unserer Turtle-Funktionen-Liste](../Cheatsheets/Turtle_Funktionen.md) nach, wie das geht).

Das kann natürlich ein ganz normaler Zacken-Stern sein, aber auch sowas hier, was noch ein bisschen mehr sparkly ist:  
![W4_Stern_hell.png](..%2FAbbildungen%2FW4_Stern_hell.png)

Für die runden Teile braucht Ihr Viertelkreise mit `circle()` und 180-Grad-Wenden. 

<details><summary>Tipp: Ich krieg keinen Stern, sondern einen Kreis raus...</summary>
Malt die Form erstmal mit Stift auf Papier. Dann baut das Programm Stück für Stück auf, führt es nach jeder hinzugefügten Zeile einmal aus und kontrolliert, ob wirklich genau das passiert, was Ihr mit Eurem Stift gemacht habt.
</details>

*Noch* sparklier wird es, wenn Ihr den oberen und unteren Stachel verlängert, wie hier beim rechten Stern:  
![W4_Sterne_lang.png](..%2FAbbildungen%2FW4_Sterne_lang.png)

### 2.2 Funktion

Macht eine Funktion draus! Kopiert die Funktion in Euer Nachthimmel-Programm und füllt Euren Nachthimmel mit Sternen!

Teilt das Ergebnis – und vielleicht auch andere Ideen, die Ihr habt – in diesem Pad:  
https://md.ha.si/ImTitYvURuaIkhmpNvmE0w?both