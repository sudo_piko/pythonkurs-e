# Generative Art mit Python

Willkommen im Kurs!

[Hier](/Protokolle/2024-04-16_Einführung.md) findet Ihr die Präsentation der Einführung.


# Für Gäste

Hi! Wenn Ihr mit diesen Materialien lernt, ohne im Kurs dabei zu sein, dann sendet mir doch mal einen lieben Gruß, 
zB auf Mastodon: @piko@chaos.social

Ich freu mich immer sehr, wenn ich mitbekomme, dass der Kurs mehr Leuten hilft!

# Den Kurs selbstständig durcharbeiten

- Sucht Euch am besten drei, vier Freund*innen, mit denen Ihr das zusammen machen könnt – das macht viel mehr Spaß und es ist leichter, dran zu bleiben. Wenn sich niemand motivieren lässt, dann fragt auf Social Media nach – zB unter #pythonkurs.  
- Der Kurs ist nach Wochen sortiert; Ihr könnt das natürlich auch schneller oder langsamer machen. Wichtig ist, dass Ihr keine zu großen Pausen macht, und am allerbesten ist natürlich, wenn Ihr jeden Tag 15 bis 30 Minuten auf den Kurs verwendet.
- Die einzelnen Wochen bestehen immer aus Videos, Quiz und Aufgaben; in dieser Reihenfolge. Teils gibt es noch Videos, die die Aufgaben besprechen.
	- Die Inhaltsvideos findet Ihr im [Videokanal für die Inhalte](https://tube.tchncs.de/c/pykurs_e/).
	- Alle Quizzes findet Ihr auf der [Webseite mit den Quizzes](https://pythonkurs.ithea.de/).
	- Alle Aufgaben findet Ihr [hier im Gitlab im Ordner "Aufgaben"](/Aufgaben).
    - Es gibt für die meisten Aufgaben auch [Aufgabenbesprechungsvideos](https://tube.tchncs.de/c/pykurs_e_aufgaben/videos). Schaut Euch diese Video **auf keinen Fall** an, wenn Ihr die Aufgaben noch nicht probiert habt. Diese Besprechungen sind nur dafür da, Euch Hinweise zu geben, wenn Ihr schon eine halbe Stunde lang auf der jeweiligen Aufgabe herumgekaut habt und einfach nicht weiterkommt. Im Optimalfall schaut Ihr Euch kein einziges dieser Videos an.
- Wenn Ihr Fragen habt, könnt Ihr auch jederzeit Piko kontaktieren: Auf Mastodon: @piko@chaos.social; per Mail: piko@riseup.net. Aber weil Python so verbreitet ist, kann es gut sein, dass Ihr mit einer einfachen Suche im Internet schneller gute Erklärungen findet...


Gefördert durch die
![](https://wauland.de/images/logo-blue.svg)

# Infos

Python für absolute Anfänger*innen

- Live-Kurs 16.04.2024 bis 23.07.2024
- Dienstags, 19:30 - 21:00
- Thema: schöne Dinge bauen; generative Art




Der Pythonkurs für Absolute Anfänger\*innen geht ab April 2024 in die nächste Runde. Eingeladen sind
ganz besonders die, die sich für einen hoffnungslosen Fall halten. Wir fangen bei **null** Programmiererfahrung an. 
Der Kurs wird sehr übungslastig sein; der Fokus des Kurses liegt darauf, schöne Dinge automatisch erstellen zu lassen,
also Generative Kunst. Ich habe der Wau-Holland-Stiftung[1] zu danken, deren Förderung mir ermöglicht, den Kurs kostenlos anzubieten!

[1] https://wauland.de/de/

