# Funktionen der Turtle

Dies ist eine Sammlung von Funktionen aus dem Turtle-Modul. Wenn Ihr Änderungen oder Ergänzungen habt, dann schreibt gerne an Piko.  
Hier hatten wir das gesammelt: https://md.ha.si/yTjw2XtJRCGPD9kcgXOJOA?both

## forward
Lässt die Turtle um "zahl" Pixel nach vorne laufen

## pensize
Verändert die Stiftdicke, mit der dann zB bei `forward` gemalt wird. `pensize(1)` lässt den Stift 1 Pixel dick sein; `pensize(10)` lässt ihn 10 Pixel dick sein.

## speed 
speed(1) ist die geringste Geschwindigkeit, mit der sich der Befehl ausführt. 
Verwendung: ich gebe zuerst die Geschwindigkeit speed mit einer Zahl in Klammern an, dann in der nächsten Zeile gebe ich an, was mit dieser Geschwindigkeit gemacht werden soll. Je höher die Zahl desto schneller wird z.B. der Pfeil gezeichnet. Und ja, wir brauchen eine Zahl in Klammern, ohne Zahl geht es nicht.

speed(0) ist die höchste Geschwindigkeit


## circle
malt nen kreis!
circle(radius)
circle(radius, Winkel)
circle(radius, Winkel, Anzahl der Ecken) *_*
circle(100,360,4) - Quadrat (eckiger Kreis) geht auch
Quelle: https://www.geeksforgeeks.org/draw-circle-in-python-using-turtle/


## bgcolor 

**Hier ist Piko ein Fehler durchgerutscht: Die Turtle verwendet Zahlen zwischen 0 und 1 und nicht zwischen 0 und 255. Eine ausführliche Erklärung findet Ihr auch bei color**

backgroundcolor z.B. in Klammern und Anführungszeichen z.B. "red" / bgcolor(“color”) oder die RGB Color Code Zahlen z.B. (1,0,0) / bgcolor(r, g, b):
RGB defines the values of red (the first number), green (the second number), or blue (the third number). The number 0 signifies no representation of the color and 1 signifies the highest possible concentration of the color.
Funny fact: white is bgcolor(1,1,1), black is bgcolor(0,0,0)
(Siehe auch color)

## setheading
Dreht den Pfeil in eine Richtung (Gradangabe) ohne ihn zu bewegen oder eine Linie zu zeichnen

## setx, sety
Position auf x(horizontal)- und y(vertikal)-Achse  (Quelle https://holypython.com/python-turtle-tutorial/turtle-positioning-goto-setpos-setx-sety-setheading/?utm_content=cmp-true  oder: https://docs.python.org/3/library/turtle.html##turtle.setx)
 die Turtle/Cursor bewegt sich zu dem angegebenen x- bzw y-Wert und führt von dort den nächsten Befehl aus
wenn ich setx / sety benutze, ohne vorher penup zu verwenden, zeichnet die turtle eine linie zu dem x oder y, das ich angebe

## goto
 --> mit goto springen wir an eine bestimmte Position im turtle-Fenster, wir müssen dazu in Klammern zwei Koordinaten angeben, getrennt mit einem Komma. Diese können sowohl positiv als auch negativ sein. z.B: `goto(70,-20)`
 
## dot
 --> `dot(50)` es wird ein (gefüllter) Kreis von der Turtle an der momentanen position gezeichnet mit der größe 50
 
## penup, pendown
Benötigen beide einen weiteren Befehl;
Hier wird sozusagen der "Stift vom Papier" genommen (penup) und wieder "aufs Papier gesetzt" (pendown); zwischen die Klammern kommt nichts.
Beispiel:
```python
forward(30)
penup()
forward(10)
pendown()
forward(30)
```
 -> ergibt eine gestrichelte Linie
 
 
## color
Definiert die Farbe der Linie.
Die erste Koordinate ist der Rot-Wert, die zweite Koordinate ist der Grün-Wert, die dritte Koordinate ist der Blau-Wert.
Die Zahlenwerte können zwischen 0 und 1 liegen und entsprechen der Sättigung des jeweiligen Farbwerts, was eine andere Form der Darstellung für 0 bis 100% ist. "0.5" ist also einfach eine Darstellung für "50%"
Im Beispiel oben ist also die Sättigung für alle drei Werte 50%.

Beispiele:
```
color(0, 0, 0) = schwarz
color(1, 1, 1) = weiß
color(0.5, 0.5, 0.5) = "mittelgrau" (Sind alle drei Werte identisch, hat man eine Graustufe, je kleiner die Zahlen, desto dunkler das Grau.)
color(1, 0, 0) = rot
color(0, 1, 0) = grün
color(0, 0, 1) = blau
```
Andere Möglichkeiten, die Farben zu definieren, wurde schon bei "backgroundcolor" beschrieben. Werte mit "##" gehen auch, sowas wie ##FFFFFF für weiß. 
Es können auch Wörter für Farben gesetzt werden, wie "cyan". Hexwerte und Wörter müssen in Anführungszeichen gesetzt werden.




## begin_fill, end_fill
hier mit kann man eine Form füllen
dafür importiert man erst turtle ganz normal, dann fängt man das füllen an, zeichnet eine Form und dann beendet man den fill
Beispiel:
```python
from turtle import *
color("red", "green")
begin_fill()
circle(100)
end_fill()
```

## reset vs clear
`clear()`:
Löscht das Bild vom Bildschirm, bewegt die Turtle aber nicht. Das heißt, der Cursor bleibt da, wo er stand.
`reset()`:
Löscht das Bild und setzt die Turtle, also den Cursor, wieder an die Ausgangsposition.

Beides braucht kein Argument (also es muss nichts zwischen den Klammern hin)


## shape und stamp
Quelle für stamp: https://www.geeksforgeeks.org/turtle-stamp-function-in-python/
Mit shape kann man die Form des Symbols, das den Turtle bewegt, ändern. Man kann sie zb in eine Schildkröte ändern, dann lautet der Befehl shape('turtle')
stamp macht einen "Stempel", z.B. das gewählte Symbol turtle, jedes Mal wenn das Biest einen neuen Befehl (Richtung z.B.) folgt. Hierfür lautet der Befehl stamp().
Beispiel:
```
from turtle import *
forward(50)
left(45)
forward(50)
stamp()
forward(30)
shape('turtle')
forward(30)
left(45)
forward(50)
stamp()
forward(50)
right(90)
forward(50)
```

überall, wo `stamp()` steht, erscheint ein Pfeil oder eine Schildkröte auf der Leinwand, bevor der nächste Befehl weitergeht.


## hideturtle und showturtle
Versteckt den Pfeil (oder was auch immer mit `shape()` gemacht wurde) bzw zeigt ihn wieder an.